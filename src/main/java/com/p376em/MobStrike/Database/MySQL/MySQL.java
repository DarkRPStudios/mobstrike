package com.p376em.MobStrike.Database.MySQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.p376em.MobStrike.MobStrike;


public class MySQL
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	 
	 public static final String ANSI_RESET = "\033[0m";
	 public static final String ANSI_GREEN = "\033[32m";
	 public static final String ANSI_RED = "\033[31m";
	 public static final String ANSI_YELLOW = "\033[33m";
	 	 
	 private Connection connection;
	 Boolean enabled = plugin.getDatabaseConfig().getBoolean("Database.Mysql.Enabled");
	 String host;
	 int port;
	 String database;
	 String username;
	 String password;
	 Boolean usessl;
	 
	 public void setValues()
	 {
		 enabled = plugin.getDatabaseConfig().getBoolean("Database.Mysql.Enabled");
		 host = plugin.getDatabaseConfig().getString("Database.Mysql.Host");
		 port = plugin.getDatabaseConfig().getInt("Database.Mysql.Port");
		 database = plugin.getDatabaseConfig().getString("Database.Mysql.Database");
		 username = plugin.getDatabaseConfig().getString("Database.Mysql.Username");
		 password = plugin.getDatabaseConfig().getString("Database.Mysql.Password");
		 usessl = plugin.getDatabaseConfig().getBoolean("Database.Mysql.UseSSL");
	 }
	 
	 public void databaseExists()
	 {
		 isDatabaseAccessable();
	 }
	 
	 private void isDatabaseAccessable()
	 {
		 try
		 {
			 synchronized (this)
			 {
		          if ((getConnection() != null) && (!getConnection().isClosed()))
		          {
		            return;
		          }
		          
		          Class.forName("com.mysql.jdbc.Driver");
		          DriverManager.setLoginTimeout(5);
		          setConnection(DriverManager.getConnection("jdbc:mysql://" +  host + ":" +  port + "/" + database + "?autoReconnect=true&useSSL=" + usessl, username, password));
		          
		          System.out.println("\033[32mDatabase: MySQL Connection Successful\033[0m");
			 }
		        return;
		 }
		 catch (SQLException e)
		 {
			 System.out.println("\033[31mDatabase: MySQL Connection Failed\033[0m");
		 }
		 catch (ClassNotFoundException e)
		 {
			 System.out.println("\033[31mDatabase: MySQL Connection Failed\033[0m");
		 }
	}

	  public Connection getConnection()
	  {
	    return this.connection;
	  }
	  
	  public void setConnection(Connection connection)
	  {
	    this.connection = connection;
	  }
	
	
}
