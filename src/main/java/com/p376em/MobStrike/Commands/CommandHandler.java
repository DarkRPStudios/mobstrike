package com.p376em.MobStrike.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.Commands.SubCommands.Edit.addArmouryItemCommand;
import com.p376em.MobStrike.Commands.SubCommands.Edit.addShopItemCommand;
import com.p376em.MobStrike.Commands.SubCommands.Edit.bungeeCommand;
import com.p376em.MobStrike.Commands.SubCommands.Edit.bungeeLobbyCommand;
import com.p376em.MobStrike.Commands.SubCommands.Edit.openArmouryInventory;
import com.p376em.MobStrike.Commands.SubCommands.Edit.openShopInventory;
import com.p376em.MobStrike.Commands.SubCommands.Edit.testCommand;
import com.p376em.MobStrike.Commands.SubCommands.Setup.createArmouryInventoryCommand;
import com.p376em.MobStrike.Commands.SubCommands.Setup.createCommand;
import com.p376em.MobStrike.Commands.SubCommands.Setup.createShopInventoryCommand;
import com.p376em.MobStrike.Commands.SubCommands.Setup.setArmouryNPCCommand;
import com.p376em.MobStrike.Commands.SubCommands.Setup.setArmourySpawnCommand;
import com.p376em.MobStrike.Commands.SubCommands.Setup.setBlueArenaCommand;
import com.p376em.MobStrike.Commands.SubCommands.Setup.setBlueCommand;
import com.p376em.MobStrike.Commands.SubCommands.Setup.setBlueExperienceOrbsSpawn;
import com.p376em.MobStrike.Commands.SubCommands.Setup.setBlueMobSpawnCommand;
import com.p376em.MobStrike.Commands.SubCommands.Setup.setGameCommand;
import com.p376em.MobStrike.Commands.SubCommands.Setup.setLobbyCommand;
import com.p376em.MobStrike.Commands.SubCommands.Setup.setRedArenaCommand;
import com.p376em.MobStrike.Commands.SubCommands.Setup.setRedCommand;
import com.p376em.MobStrike.Commands.SubCommands.Setup.setRedExperienceOrbsSpawn;
import com.p376em.MobStrike.Commands.SubCommands.Setup.setRedMobSpawnCommand;
import com.p376em.MobStrike.Commands.SubCommands.Setup.setShopNPCCommand;

import java.util.HashMap;
import java.util.Map;

public class CommandHandler implements org.bukkit.command.CommandExecutor
{

    private Map<String, CommandExecutor> commands = new HashMap<>();

    public CommandHandler()
    {
    	commands.put("addshopitem", new addShopItemCommand());
        commands.put("openshop", new openShopInventory());
        commands.put("addarmouryitem", new addArmouryItemCommand());
        commands.put("openarmoury", new openArmouryInventory());
        commands.put("setbungeelobby", new bungeeLobbyCommand());
        commands.put("setbungee", new bungeeCommand());
    	commands.put("creategame", new createCommand());
        commands.put("createshop", new createShopInventoryCommand());
        commands.put("createarmoury", new createArmouryInventoryCommand());
        commands.put("setarmouryspawn", new setArmourySpawnCommand());
        commands.put("setbluearena", new setBlueArenaCommand());
        commands.put("setbluespawn", new setBlueCommand());
        commands.put("setblueexp", new setBlueExperienceOrbsSpawn());
        commands.put("setbluemob", new setBlueMobSpawnCommand());
        commands.put("setgame", new setGameCommand());
        commands.put("setlobby", new setLobbyCommand());
        commands.put("setshopnpc", new setShopNPCCommand());
        commands.put("setarmourynpc", new setArmouryNPCCommand());
        commands.put("setredarena", new setRedArenaCommand());
        commands.put("setredspawn", new setRedCommand());
        commands.put("setredexp", new setRedExperienceOrbsSpawn());
        commands.put("setredmob", new setRedMobSpawnCommand());
        commands.put("test", new testCommand());
        
    }

    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args)
    {
        if (cmd.getName().equalsIgnoreCase("mobstrike"))
        {

            if (args.length == 0) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("&7&m--------------------\n");
                for (CommandExecutor command : commands.values())
                {

                    if (command.getPermission() != null && !sender.hasPermission(command.getPermission()))
                    {
                        stringBuilder.append("");
                    } else {
                        stringBuilder.append("&e- &7").append(command.getCommand()).append("\n");
                    }

                }
                stringBuilder.append("&7&m--------------------");
                return true;
            }

            if (args[0] != null)
            {
                String name = args[0].toLowerCase();
                if (commands.containsKey(name))
                {
                    final CommandExecutor command = commands.get(name);

                    if (command.getPermission() != null && !sender.hasPermission(command.getPermission()))
                    {
                        sender.sendMessage(ChatColor.RED + "You do not have permission to use this command!");
                        return true;
                    }

                    if (!command.isBoth())
                    {
                        if (command.isConsole() && sender instanceof Player) {
                            sender.sendMessage(ChatColor.RED + "Only console can use that command!");
                            return true;
                        }
                        if (command.isPlayer() && sender instanceof ConsoleCommandSender) {
                            sender.sendMessage(ChatColor.RED + "Only players can use that command!");
                            return true;
                        }
                    }

                    if (command.getLength() != args.length)
                    {
                        sender.sendMessage(ChatColor.RED + "Usage: " + command.getUsage());
                        return true;
                    }

                    command.execute(sender, args);
                }
            }
        }
        return false;
    }

    public Map<String, CommandExecutor> getCommands()
    {
        return commands;
    }

}