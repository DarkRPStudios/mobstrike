package com.p376em.MobStrike.Commands;

import org.bukkit.command.CommandSender;

public abstract class CommandExecutor
{
  private String command;
  private String permission;
  private String usage;
  private boolean console;
  private boolean player;
  private int length;
  
  public abstract void execute(CommandSender paramCommandSender, String[] paramArrayOfString);
  
  public void setBoth()
  {
    this.player = true;
    this.console = true;
  }
  
  public boolean isBoth()
  {
    return (this.player) && (this.console);
  }
  
  public String getCommand()
  {
    return this.command;
  }
  
  public void setCommand(String command)
  {
    this.command = command;
  }
  
  public String getPermission()
  {
    return this.permission;
  }
  
  public void setPermission(String permission)
  {
    this.permission = permission;
  }
  
  public boolean isConsole()
  {
    return this.console;
  }
  
  public void setConsole()
  {
    this.console = true;
    this.player = false;
  }
  
  public boolean isPlayer()
  {
    return this.player;
  }
  
  public void setPlayer()
  {
    this.player = true;
    this.console = false;
  }
  
  public int getLength()
  {
    return this.length;
  }
  
  public void setLength(int length)
  {
    this.length = length;
  }
  
  public String getUsage()
  {
    return this.usage;
  }
  
  public void setUsage(String usage)
  {
    this.usage = usage;
  }
}
