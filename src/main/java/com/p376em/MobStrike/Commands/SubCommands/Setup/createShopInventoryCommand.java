package com.p376em.MobStrike.Commands.SubCommands.Setup;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Commands.CommandExecutor;
import com.p376em.MobStrike.Utils.StringUtils;


public class createShopInventoryCommand extends CommandExecutor
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration shop = null;
	
    public createShopInventoryCommand()
    {
        setCommand("createshop");
        setLength(3);
        setUsage("/mobstrike createshop <Shop ID> <Inventory Slots>");
        setPlayer();
        setPermission("mobstrike.admin.create");
    }
    

    @Override
    public void execute(CommandSender sender, String[] args)
    {
    	shop = plugin.getShopConfig(); 
    	Player player = (Player) sender;
    	
    	Integer shopID = Integer.valueOf(args[1]);
    	Integer inventorySlots = Integer.valueOf(args[2]);
    	String inventoryName = "The Shop Name Can Include Colour Codes";
    	String npcName = "The name of the shop npc";
    	 	
    	if(inventorySlots % 9 != 0)
    	{
    		sender.sendMessage(StringUtils.color("&4The inventory slots must be a multiple of nine"));
    		return;
    	}
    	else if(shopID < 0)
    	{
    		sender.sendMessage(StringUtils.color("&4The shop ID must be larger than zero"));
    		return;
    	}
    	
    	if(shop.getString("Shop." + shopID) != null)
    	{
    		player.sendMessage(StringUtils.color("&4A shop with the id: " + shopID + " already exists\nPlease edit this shop or use another shop id"));
    	}
    	else
    	{
    		shop.set("Shop." + shopID + ".Settings.ShopID", shopID);
    		shop.set("Shop." + shopID + ".Settings.shopName", inventoryName);
    		shop.set("Shop." + shopID + ".Settings.shopNPCName", npcName);
    		shop.set("Shop." + shopID + ".Settings.inventorySlots", inventorySlots);

    		plugin.saveShopConfig(shop);
    		shop = null;

    		player.sendMessage(StringUtils.color("&6Shop with the ID: " + shopID + " has been created successfully"));
    		player.sendMessage(StringUtils.color("&6Now please add items by using /mobstrike addshopitem " + shopID));
    	}
    }
}