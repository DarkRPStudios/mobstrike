package com.p376em.MobStrike.Commands.SubCommands.Setup;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Commands.CommandExecutor;
import com.p376em.MobStrike.Utils.StringUtils;


public class setArmourySpawnCommand extends CommandExecutor
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration games = null;
	
    public setArmourySpawnCommand()
    {
        setCommand("setarmouryspawn");
        setLength(2);
        setUsage("/mobstrike setarmouryspawn <GameID>");
        setPlayer();
        setPermission("mobstrike.admin.create");
    }
    
    @Override
    public void execute(CommandSender sender, String[] args)
    {
    	games = plugin.getGamesConfig();
    	
    	Player player = (Player) sender;
    	Integer gameID = Integer.valueOf(args[1]);
    	    	
    	String world = player.getLocation().getWorld().getName();
    	int x = player.getLocation().getBlockX();
    	int y = player.getLocation().getBlockY();
    	int z = player.getLocation().getBlockZ();
		int armoury = games.getInt("Games." + gameID + ".Settings.MaxPlayers");
    	int i = 0;    
    	
    	for(i = 1; i <= armoury; i++)
    	{
    		if(games.getString("Games." + gameID + ".Armoury." + i + ".world") == null)
    		{
    			games.set("Games." + gameID + ".Armoury." + i + ".world", world);
    			games.set("Games." + gameID + ".Armoury." + i + ".x", x);
    			games.set("Games." + gameID + ".Armoury." + i + ".y", y);
    			games.set("Games." + gameID + ".Armoury." + i + ".z", z);
    	    	
    	    	plugin.saveGamesConfig(games);
    	    	games = null;
    	    	player.sendMessage(StringUtils.color("&5Blue team spawn point &7(" + i + "/" + armoury + ") &5for game " + gameID + " has been set Successfully"));
    	    	
    	    	if(i == armoury)
    	    	{
    	    		player.sendMessage(StringUtils.color("&6Now please set the npc spawn locations using /mobstrike setnpc " + gameID));
    	    	}
    	    	return;
    		}
    	}
    }
}