package com.p376em.MobStrike.Commands.SubCommands.Setup;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Commands.CommandExecutor;
import com.p376em.MobStrike.Utils.StringUtils;


public class setBlueMobSpawnCommand extends CommandExecutor
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration games = null;
	
    public setBlueMobSpawnCommand()
    {
        setCommand("setbluemob");
        setLength(4);
        setUsage("/mobstrike setbluemob <GameID> <Land/Water> <Mobs Use : instead of spaces>");
        setPlayer();
        setPermission("mobstrike.admin.create");
    }
    
    @Override
    public void execute(CommandSender sender, String[] args)
    {
    	games = plugin.getGamesConfig();
    	
    	Player player = (Player) sender;
    	Integer gameID = Integer.valueOf(args[1]);
    	
    	String landType = args[2];
    	String world = player.getLocation().getWorld().getName();
    	int x = player.getLocation().getBlockX();
    	int y = player.getLocation().getBlockY();
    	int z = player.getLocation().getBlockZ();
    	String allowedMobs = args[3].toUpperCase();
    	int mobSpawns = 20;
    	int i = 0;
    	
	    	for(i = 1; i <= mobSpawns; i++)
	    	{
	    		if(games.getString("Games." + gameID + ".Mobs.Blue." + landType + "." + i + ".world") == null)
	    		{
	    			games.set("Games." + gameID + ".Mobs.Blue." + landType + "." + i + ".world", world);
	    			games.set("Games." + gameID + ".Mobs.Blue." + landType + "." + i + ".x", x);
	    			games.set("Games." + gameID + ".Mobs.Blue." + landType + "." + i + ".y", y);
	    			games.set("Games." + gameID + ".Mobs.Blue." + landType + "." + i + ".z", z);
	    			games.set("Games." + gameID + ".Mobs.Blue." + landType + "." + i + ".AllowedMobs", allowedMobs);
	    	    	
	    	    	plugin.saveGamesConfig(games);
	    	    	games = null;
	    	    	player.sendMessage(StringUtils.color("&5Mob spawn point &7(" + i + "/" + mobSpawns + ") &5for game " + gameID + " has been set Successfully"));
	    	    	
	    	    	if(i == mobSpawns)
	    	    	{
	    	    		player.sendMessage(StringUtils.color("&6Now please set the boss spawn points using /assassin setboss " + gameID));
	    	    	}
	    	    	return;
	    		}
	    	}	
    }
}