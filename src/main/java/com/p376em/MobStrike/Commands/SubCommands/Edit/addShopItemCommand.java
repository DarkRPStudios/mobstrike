package com.p376em.MobStrike.Commands.SubCommands.Edit;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Commands.CommandExecutor;
import com.p376em.MobStrike.Utils.StringUtils;

public class addShopItemCommand extends CommandExecutor
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration shop = null;
	
   public addShopItemCommand()
   {
       setCommand("addshopitem");
       setLength(10);
       setUsage("/mobstrike addshopitem <Shop ID> <Item ID> <Name> <Cooldown> <EXP Levels> <Price> <Slot Number> <Material> <TriggerBlock>");
       setPlayer();
       setPermission("mobstrike.admin.shop");
   }

   @Override
   public void execute(CommandSender sender, String[] args)
   {   	
	   Player player = (Player) sender;
	   
	   Integer shopID = Integer.valueOf(args[1]);
	   Integer itemID = Integer.valueOf(args[2]);
	   String name = args[3].replaceAll("_", " ");
	   Integer cooldown = Integer.valueOf(args[4]);
	   Integer exp = Integer.valueOf(args[5]);
	   Integer price = Integer.valueOf(args[6]);
	   Integer slot = Integer.valueOf(args[7]);
	   String item = args[8].toUpperCase(); 
	   String trigger = args[9].toUpperCase(); 
	   String permission = "mobstrike.shopmobs." + args[3]; 
	   
	   shop = plugin.getShopConfig();
	   
	   if(shop.getString("Shop." + shopID + ".Items." + itemID + ".Name") == null)
	   {
		   if(shop.getInt("Shop." + shopID + ".Settings.ShopID") > 0)
		   {
			   shop.set("Shop." + shopID + ".Items." + itemID + ".Name", name);
			   shop.set("Shop." + shopID + ".Items." + itemID + ".Cooldown", cooldown);
			   shop.set("Shop." + shopID + ".Items." + itemID + ".EXPLevels", exp);
			   shop.set("Shop." + shopID + ".Items." + itemID + ".Price", price);
			   shop.set("Shop." + shopID + ".Items." + itemID + ".ItemSlot", slot);
			   shop.set("Shop." + shopID + ".Items." + itemID + ".Item", item);
			   shop.set("Shop." + shopID + ".Items." + itemID + ".Permission", permission);
			   shop.set("Shop." + shopID + ".Items." + itemID + ".TriggerBlock", trigger);
			   shop.set("Shop." + shopID + ".Items." + itemID + ".Effect", "Please change this text in the shop config");
			   
			   plugin.saveShopConfig(shop);
			   shop = null;
			   
			   player.sendMessage(StringUtils.color("&2Successfully added the " + name + " to the shop in slot " + slot));
		   }
	   }
	   else
	   {
		   player.sendMessage(StringUtils.color("&4An item already exists with the item ID: " + itemID + "\nPlease use another item ID number"));
	   }
   	}
}