package com.p376em.MobStrike.Commands.SubCommands.Edit;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Commands.CommandExecutor;
import com.p376em.MobStrike.Utils.StringUtils;


public class bungeeLobbyCommand extends CommandExecutor
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration settings = null;
   public bungeeLobbyCommand()
   {
       setCommand("setbungeelobby");
       setLength(2);
       setUsage("/assassin setbungeelobby <Bungeecord Lobby Name>");
       setBoth();
       setPermission("assassin.admin.create");
   }

   @Override
   public void execute(CommandSender sender, String[] args)
   {   	
	   	Player player = (Player) sender;
	   	String lobbyName = args[1];
	   	
	   	settings = plugin.getSettingsConfig();
	   	settings.set("Settings.Bungeecord.GameEndLobby", lobbyName);
	   	plugin.saveSettingsConfig(settings);
	   	settings = null;
	   	
	   	player.sendMessage(StringUtils.color("&2Bungeecord lobby has been set to " + lobbyName));
   }
}