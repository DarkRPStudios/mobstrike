package com.p376em.MobStrike.Commands.SubCommands.Setup;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Commands.CommandExecutor;
import com.p376em.MobStrike.Utils.StringUtils;


public class setBlueArenaCommand  extends CommandExecutor
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration games = null;	
	
    public setBlueArenaCommand()
    {
        setCommand("setbluearena");
        setLength(2);
        setUsage("/mobstrike setbluearena <GameID>");
        setPlayer();
        setPermission("mobstrike.admin.create");
    }
    
    @Override
    public void execute(CommandSender sender, String[] args)
    {
    	games = plugin.getGamesConfig();
    	
    	Player player = (Player) sender;
    	Integer gameID = Integer.valueOf(args[1]);
    	
    	String world = player.getLocation().getWorld().getName();
    	int x = player.getLocation().getBlockX();
    	int y = player.getLocation().getBlockY();
    	int z = player.getLocation().getBlockZ();
    	
    	if(games.getString("Games." + gameID + ".Arena.Blue." + 1 + ".world") == null)
    	{  
			games.set("Games." + gameID + ".Arena.Blue." + 1 + ".world", world);
	    	games.set("Games." + gameID + ".Arena.Blue." + 1 + ".x", x);
	    	games.set("Games." + gameID + ".Arena.Blue." + 1 + ".y", y);
	    	games.set("Games." + gameID + ".Arena.Blue." + 1 + ".z", z);
	    	
	    	plugin.saveGamesConfig(games);
	    	games = null;
	    	
	    	player.sendMessage(StringUtils.color("&5The first blue arena corner for game " + gameID + " has been set Successfully"));
	    	return;
    	}
    	else if(games.getString("Games." + gameID + ".Arena.Blue." + 2 + ".world") == null)
    	{  
			games.set("Games." + gameID + ".Arena.Blue." + 2 + ".world", world);
	    	games.set("Games." + gameID + ".Arena.Blue." + 2 + ".x", x);
	    	games.set("Games." + gameID + ".Arena.Blue." + 2 + ".y", y);
	    	games.set("Games." + gameID + ".Arena.Blue." + 2 + ".z", z);
	    	
	    	plugin.saveGamesConfig(games);
	    	games = null;
	    	
	    	player.sendMessage(StringUtils.color("&5The second blue arena corner for game " + gameID + " has been set Successfully"));
	    	
	    	setBlocks(gameID, player);
	    	return;
    	}
    	else
    	{
    		player.sendMessage(StringUtils.color("&5The blue arena corner for game " + gameID + " could not be set"));
    	}
    }
    
	public void setBlocks(int gameID, Player player)
    {
    	games = plugin.getGamesConfig();
    	String loc1World = games.getString("Games." + gameID + ".Arena.Blue." + 1 + ".world");
    	String loc2World = games.getString("Games." + gameID + ".Arena.Blue." + 2 + ".world");
    	int loc1x = games.getInt("Games." + gameID + ".Arena.Blue." + 1 + ".x");
		int loc2x = games.getInt("Games." + gameID + ".Arena.Blue." + 2 + ".x");
		int loc1y = games.getInt("Games." + gameID + ".Arena.Blue." + 1 + ".y") - 1;
		int loc1z = games.getInt("Games." + gameID + ".Arena.Blue." + 1 + ".z");
		int loc2z = games.getInt("Games." + gameID + ".Arena.Blue." + 2 + ".z");
    	
    	int right = 0;
    	int down = 0;
    	int locY = 0;
    	
    	if(loc1World.equalsIgnoreCase(loc2World))
    	{
    		Location loc1 = new Location(Bukkit.getServer().getWorld(loc1World), loc1x, loc1y, loc1z);
			Block block1 = loc1.getBlock();
			block1.setType(Material.GRASS);
    		
    		if(loc1x > loc2x)
    		{
    			for(right = loc2x; right < loc1x; right++)
    			{
    				Location loc = new Location(Bukkit.getServer().getWorld(loc1World), right, loc1y, loc1z);
    				Block block = loc.getBlock();
    				block.setType(Material.GRASS);
    				
    			}
    		}
    		else if(loc1x < loc2x)
    		{
    			for(right = loc1x; right < loc2x; right++)
    			{
    				Location loc = new Location(Bukkit.getServer().getWorld(loc1World), right, loc1y, loc1z);
    				Block block = loc.getBlock();
    				block.setType(Material.GRASS);
    				
    			}
    		}
    		
    		if(loc1x > loc2x)
    		{
    			for(right = loc2x; right < loc1x; right++)
    			{
    				Location loc = new Location(Bukkit.getServer().getWorld(loc1World), right, loc1y, loc2z);
    				Block block = loc.getBlock();
    				block.setType(Material.GRASS);
    				
    			}
    		}
    		else if(loc1x < loc2x)
    		{
    			for(right = loc1x; right < loc2x; right++)
    			{
    				Location loc = new Location(Bukkit.getServer().getWorld(loc1World), right, loc1y, loc2z);
    				Block block = loc.getBlock();
    				block.setType(Material.GRASS);
    				
    			}
    		}
    		
    		if(loc1z > loc2z)
    		{
    			for(down = loc2z; down < loc1z; down++)
    			{
    				Location loc = new Location(Bukkit.getServer().getWorld(loc1World), loc1x, loc1y, down);
    				Block block = loc.getBlock();
    				block.setType(Material.GRASS);
    				
    			}
    		}
    		else if(loc1z < loc2z)
    		{
    			for(down = loc1z; down < loc2x; down++)
    			{
    				Location loc = new Location(Bukkit.getServer().getWorld(loc1World), loc1x, loc1y, down);
    				Block block = loc.getBlock();
    				block.setType(Material.GRASS);
    				
    			}
    		}
    		
    		if(loc1z > loc2z)
    		{
    			for(down = loc2z; down < loc1z; down++)
    			{
    				Location loc = new Location(Bukkit.getServer().getWorld(loc1World), loc2x, loc1y, down);
    				Block block = loc.getBlock();
    				block.setType(Material.GRASS);
    				
    			}
    		}
    		else if(loc1z < loc2z)
    		{
    			for(down = loc1z; down < loc2z; down++)
    			{
    				Location loc = new Location(Bukkit.getServer().getWorld(loc1World), loc2x, loc1y, down);
    				Block block = loc.getBlock();
    				block.setType(Material.GRASS);
    				
    			}
    		}
    		
    		for(locY = 1; locY >= 50; locY++)
    		{
        		Location locy1 = new Location(Bukkit.getServer().getWorld(loc1World), loc1x, loc1y + locY, loc1z);
    			Block blocky1 = locy1.getBlock();
    			blocky1.setType(Material.BARRIER);
        		
        		if(loc1x > loc2x)
        		{
        			for(right = loc2x; right < loc1x; right++)
        			{
        				Location loc = new Location(Bukkit.getServer().getWorld(loc1World), right, loc1y + locY, loc1z);
        				Block block = loc.getBlock();
        				block.setType(Material.BARRIER);
        				
        			}
        		}
        		else if(loc1x < loc2x)
        		{
        			for(right = loc1x; right < loc2x; right++)
        			{
        				Location loc = new Location(Bukkit.getServer().getWorld(loc1World), right, loc1y + locY, loc1z);
        				Block block = loc.getBlock();
        				block.setType(Material.BARRIER);
        				
        			}
        		}
        		
        		if(loc1x > loc2x)
        		{
        			for(right = loc2x; right < loc1x; right++)
        			{
        				Location loc = new Location(Bukkit.getServer().getWorld(loc1World), right, loc1y + locY, loc2z);
        				Block block = loc.getBlock();
        				block.setType(Material.BARRIER);
        				
        			}
        		}
        		else if(loc1x < loc2x)
        		{
        			for(right = loc1x; right < loc2x; right++)
        			{
        				Location loc = new Location(Bukkit.getServer().getWorld(loc1World), right, loc1y + locY, loc2z);
        				Block block = loc.getBlock();
        				block.setType(Material.BARRIER);
        				
        			}
        		}
        		
        		if(loc1z > loc2z)
        		{
        			for(down = loc2z; down < loc1z; down++)
        			{
        				Location loc = new Location(Bukkit.getServer().getWorld(loc1World), loc1x, loc1y + locY, down);
        				Block block = loc.getBlock();
        				block.setType(Material.BARRIER);
        				
        			}
        		}
        		else if(loc1z < loc2z)
        		{
        			for(down = loc1z; down < loc2x; down++)
        			{
        				Location loc = new Location(Bukkit.getServer().getWorld(loc1World), loc1x, loc1y + locY, down);
        				Block block = loc.getBlock();
        				block.setType(Material.BARRIER);
        				
        			}
        		}
        		
        		if(loc1z > loc2z)
        		{
        			for(down = loc2z; down < loc1z; down++)
        			{
        				Location loc = new Location(Bukkit.getServer().getWorld(loc1World), loc2x, loc1y + locY, down);
        				Block block = loc.getBlock();
        				block.setType(Material.BARRIER);
        				
        			}
        		}
        		else if(loc1z < loc2z)
        		{
        			for(down = loc1z; down < loc2z; down++)
        			{
        				Location loc = new Location(Bukkit.getServer().getWorld(loc1World), loc2x, loc1y + locY, down);
        				Block block = loc.getBlock();
        				block.setType(Material.BARRIER);
        				
        			}
        		}
    		}
    		

    		player.sendMessage(StringUtils.color("&6Now please set the blue arena using /mobstrike setbluearena " + gameID));
    	}
    }
}