package com.p376em.MobStrike.Commands.SubCommands.Setup;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Commands.CommandExecutor;
import com.p376em.MobStrike.Utils.StringUtils;


public class setGameCommand extends CommandExecutor
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration settings = plugin.getSettingsConfig();
	FileConfiguration games = plugin.getGamesConfig();
	FileConfiguration shop = plugin.getShopConfig();
	FileConfiguration armoury = plugin.getArmouryConfig();
	
   public setGameCommand()
   {
       setCommand("setgame");
       setLength(4);
       setUsage("/mobstrike setgame <GameID> <ShopID> <armouryID>");
       setPlayer();
       setPermission("mobstrike.admin.create");
   }

   @Override
   public void execute(CommandSender sender, String[] args)
   {   	
	   Player player = (Player) sender;
	   Integer gameID = Integer.valueOf(args[1]);
	   Integer shopID = Integer.valueOf(args[2]);
	   Integer armouryID = Integer.valueOf(args[3]);
	   
   		if(games.getInt("Games." + gameID + ".Settings.GameID") > 0 && shop.getInt("Shop." + shopID + ".Settings.ShopID") > 0 && armoury.getInt("Armoury." + armouryID + ".Settings.ArmouryID") > 0)
   		{	   
   			settings.set("Settings.Games.PlayGameID", gameID);
   			settings.set("Settings.Games.ShopID", shopID);
   			settings.set("Settings.Games.ArmouryID", armouryID);
   			settings.set("Settings.Games.FriendlyFire", false);
   			plugin.saveSettingsConfig(settings);   	
   	
   			for(Player onlinePlayer : Bukkit.getServer().getOnlinePlayers())
   			{
   				onlinePlayer.sendMessage(StringUtils.color("&2You have now setup Game " + gameID + " with the Shop " + shopID + "\nPlease rejoin to start a game")); 	
   			}	
   			
   			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "restart");
   		}
   		else
   		{
   			player.sendMessage(StringUtils.color("&4A game or shop that you have selected does not exist"));
   		}
   	}
}