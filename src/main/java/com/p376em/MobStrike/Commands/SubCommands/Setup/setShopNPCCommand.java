package com.p376em.MobStrike.Commands.SubCommands.Setup;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Commands.CommandExecutor;
import com.p376em.MobStrike.Utils.StringUtils;


public class setShopNPCCommand extends CommandExecutor
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration games = null;
	FileConfiguration settings = null;
	
    public setShopNPCCommand()
    {
        setCommand("setshopnpc");
        setLength(2);
        setUsage("/mobstrike setshopnpc <GameID> ");
        setPlayer();
        setPermission("mobstrike.admin.create");
    } 
    
    @Override
    public void execute(CommandSender sender, String[] args)
    {
    	games = plugin.getGamesConfig();
    	
    	Player player = (Player) sender;
    	Integer gameID = Integer.valueOf(args[1]);
    	    	
    	String world = player.getLocation().getWorld().getName();
    	double x = player.getLocation().getX();
    	double y = player.getLocation().getY();
    	double z = player.getLocation().getZ();
    	float yaw = player.getLocation().getYaw();
    	float pitch = player.getLocation().getPitch();
    	int maxNPCs = games.getInt("Games." + gameID + ".Settings.MaxPlayers");
    	int i = 0;    
    	
    	for(i = 1; i <= maxNPCs; i++)
    	{
    		if(games.getString("Games." + gameID + ".NPC.Shop." + i + ".world") == null)
    		{
    			games.set("Games." + gameID + ".NPC.Shop." + i + ".world", world);
    			games.set("Games." + gameID + ".NPC.Shop." + i + ".x", x);
    			games.set("Games." + gameID + ".NPC.Shop." + i + ".y", y);
    			games.set("Games." + gameID + ".NPC.Shop." + i + ".z", z);
    			games.set("Games." + gameID + ".NPC.Shop." + i + ".yaw", yaw);
    			games.set("Games." + gameID + ".NPC.Shop." + i + ".pitch", pitch);
    			
    	    	plugin.saveGamesConfig(games);
    	    	games = null;
    	    	player.sendMessage(StringUtils.color("&5Shop NPC spawn point &7(" + i + "/" + maxNPCs + ") &5for game " + gameID + " has been set Successfully"));
    		}
    	}
    }
}