package com.p376em.MobStrike.Commands.SubCommands.Setup;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Commands.CommandExecutor;
import com.p376em.MobStrike.Utils.StringUtils;


public class createArmouryInventoryCommand extends CommandExecutor
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration armoury = null;
	
    public createArmouryInventoryCommand()
    {
        setCommand("createarmoury");
        setLength(3);
        setUsage("/mobstrike createarmoury <Armoury ID> <Inventory Slots>");
        setPlayer();
        setPermission("mobstrike.admin.create");
    }
    

    @Override
    public void execute(CommandSender sender, String[] args)
    {
    	armoury = plugin.getArmouryConfig(); 
    	Player player = (Player) sender;
    	
    	Integer armouryID = Integer.valueOf(args[1]);
    	Integer inventorySlots = Integer.valueOf(args[2]);
    	String inventoryName = "The Armoury Name Can Include Colour Codes";
    	String npcName = "The name of the armoury npc";
    	 	
    	if(inventorySlots % 9 != 0)
    	{
    		sender.sendMessage(StringUtils.color("&4The inventory slots must be a multiple of nine"));
    		return;
    	}
    	else if(armouryID < 0)
    	{
    		sender.sendMessage(StringUtils.color("&4The armoury ID must be larger than zero"));
    		return;
    	}
    	
    	if(armoury.getString("Armoury." + armouryID) != null)
    	{
    		player.sendMessage(StringUtils.color("&4A armoury with the id: " + armouryID + " already exists\nPlease edit this armoury or use another armoury id"));
    	}
    	else
    	{
    		armoury.set("Armoury." + armouryID + ".Settings.armouryID", armouryID);
    		armoury.set("Armoury." + armouryID + ".Settings.armouryName", inventoryName);
    		armoury.set("Armoury." + armouryID + ".Settings.armouryNPCName", npcName);
    		armoury.set("Armoury." + armouryID + ".Settings.inventorySlots", inventorySlots);
    		armoury.set("Armoury." + armouryID + ".Items", null);
    		plugin.saveArmouryConfig(armoury);
    		armoury = null;

    		player.sendMessage(StringUtils.color("&6Armoury with the ID: " + armouryID + " has been created successfully"));
    		player.sendMessage(StringUtils.color("&6Now please add items by using /mobstrike addarmouryitem " + armouryID));
    	}
    }
}