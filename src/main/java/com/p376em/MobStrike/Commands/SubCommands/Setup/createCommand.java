package com.p376em.MobStrike.Commands.SubCommands.Setup;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Commands.CommandExecutor;
import com.p376em.MobStrike.Utils.StringUtils;


public class createCommand extends CommandExecutor
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration games = null;
	
    public createCommand()
    {
        setCommand("creategame");
        setLength(5);
        setUsage("/mobstrike creategame <ID> <Min Players> <Max Players> <Number of Rounds>");
        setPlayer();
        setPermission("mobstrike.admin.create");
    }
    

    @Override
    public void execute(CommandSender sender, String[] args)
    {
    	games = plugin.getGamesConfig(); 
    	Player player = (Player) sender;
    	
    	Integer gameID = Integer.valueOf(args[1]);
    	Integer minPlayers = Integer.valueOf(args[2]);
    	Integer maxPlayers = Integer.valueOf(args[3]);
    	Integer noRounds = Integer.valueOf(args[4]);
    	
    	if(minPlayers % 2 == 1)
    	{
    		player.sendMessage(StringUtils.color("&4Minimum players must be an even number"));
    		return;
    	}
    	else if(maxPlayers % 2 == 1)
    	{
    		player.sendMessage(StringUtils.color("&4Maximum players must be an even number"));
    		return;
    	}
    	else if(minPlayers < 1)
    	{
    		player.sendMessage(StringUtils.color("&4Minimum players must be 2 or greater"));
    		return;
    	}
    	else if(minPlayers > maxPlayers)
    	{
    		player.sendMessage(StringUtils.color("&4Minimum players must be Lower than the max number of players"));
    		return;
    	}  
    	
    	if(noRounds % 2 == 0)
    	{
    		player.sendMessage(StringUtils.color("&4Number of rounds must be an odd number"));
    		return;
    	}
    	
    	if(games.getString("Games." + gameID) != null)
    	{
    		player.sendMessage(StringUtils.color("&4A game with the id: " + gameID + " already exists\nPlease edit this game or use another game id"));
    	}
    	else
    	{
    		games.set("Games." + gameID + ".Settings.GameID", gameID);
    		games.set("Games." + gameID + ".Settings.MinPlayers", minPlayers);
    		games.set("Games." + gameID + ".Settings.MaxPlayers", maxPlayers);
    		games.set("Games." + gameID + ".Settings.NumberOfRounds", noRounds);

    		plugin.saveGamesConfig(games);
    		games = null;

    		player.sendMessage(StringUtils.color("&6Game with the ID: " + gameID + " has been created successfully"));
    		player.sendMessage(StringUtils.color("&6Now please set the lobby using /mobstrike setlobby " + gameID));
    	}
    }
}