package com.p376em.MobStrike.Commands.SubCommands.Edit;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.Commands.CommandExecutor;
import com.p376em.MobStrike.Managers.Inventory.InventoryManager;

public class openShopInventory extends CommandExecutor
{
	 private InventoryManager invManager = new InventoryManager();
	
	public openShopInventory()
    {
       setCommand("openshop");
       setLength(1);
       setUsage("/mobstrike openshop");
       setPlayer();
       setPermission("mobstrike.admin.shop");
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {   	
	   Player player = (Player) sender;
	   
	   invManager.shopInventory(player);
		
   	}
}