package com.p376em.MobStrike.Commands.SubCommands.Edit;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Commands.CommandExecutor;
import com.p376em.MobStrike.Utils.StringUtils;

public class addArmouryItemCommand extends CommandExecutor
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration armoury = null;
	
   public addArmouryItemCommand()
   {
       setCommand("addarmouryitem");
       setLength(7);
       setUsage("/mobstrike addarmouryitem <Armoury ID> <Item ID> <Name> <Price> <Slot Number> <Material>");
       setPlayer();
       setPermission("mobstrike.admin.shop");
   }

   @Override
   public void execute(CommandSender sender, String[] args)
   {   	
	   Player player = (Player) sender;
	   
	   Integer armouryID = Integer.valueOf(args[1]);
	   Integer itemID = Integer.valueOf(args[2]);
	   String name = args[3].replaceAll("_", " ");
	   Integer price = Integer.valueOf(args[4]);
	   Integer slot = Integer.valueOf(args[5]);
	   String item = args[6].toUpperCase(); 
	   String permission = "mobstrike.armouryitem." + args[3]; 
	  	   
	   armoury = plugin.getArmouryConfig();
	   
	   if(armoury.getString("Armoury." + armouryID + ".Items." + itemID + ".Name") == null)
	   {
		   if(armoury.getInt("Armoury." + armouryID + ".Settings.armouryID") > 0)
		   {
			   armoury.set("Armoury." + armouryID + ".Items." + itemID + ".Name", name);
			   armoury.set("Armoury." + armouryID + ".Items." + itemID + ".Price", price);
			   armoury.set("Armoury." + armouryID + ".Items." + itemID + ".ItemSlot", slot);
			   armoury.set("Armoury." + armouryID + ".Items." + itemID + ".Item", item);
			   armoury.set("Armoury." + armouryID + ".Items." + itemID + ".Permission", permission);
			   
			   plugin.saveArmouryConfig(armoury);
			   armoury = null;
			   
			   player.sendMessage(StringUtils.color("&2Successfully added the " + name + " to the armoury in slot " + slot));
		   }
	   }
	   else
	   {
		   player.sendMessage(StringUtils.color("&4An item already exists with the item ID: " + itemID + "\nPlease use another item ID number"));
	   }
   	}
}