package com.p376em.MobStrike.Commands.SubCommands.Edit;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.Commands.CommandExecutor;
import com.p376em.MobStrike.Managers.Inventory.InventoryManager;

public class openArmouryInventory extends CommandExecutor
{
	 private InventoryManager invManager = new InventoryManager();
	
	public openArmouryInventory()
    {
       setCommand("openarmoury");
       setLength(1);
       setUsage("/mobstrike openarmoury");
       setPlayer();
       setPermission("mobstrike.admin.shop");
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {   	
	   Player player = (Player) sender;
	   
	   invManager.armouryInventory(player);
		
   	}
}