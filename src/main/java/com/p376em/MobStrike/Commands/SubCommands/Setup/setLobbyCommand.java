package com.p376em.MobStrike.Commands.SubCommands.Setup;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Commands.CommandExecutor;
import com.p376em.MobStrike.Utils.StringUtils;


public class setLobbyCommand extends CommandExecutor
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration games = null;
	
    public setLobbyCommand()
    {
        setCommand("setlobby");
        setLength(2);
        setUsage("/mobstrike setLobby <GameID>");
        setPlayer();
        setPermission("mobstrike.admin.create");
    }
    
    @Override
    public void execute(CommandSender sender, String[] args)
    {
    	games = plugin.getGamesConfig();	
    	Player player = (Player) sender;
    	Integer gameID = Integer.valueOf(args[1]);
    	
    	String world = player.getLocation().getWorld().getName();
    	double x = player.getLocation().getBlockX();
    	double y = player.getLocation().getBlockY();
    	double z = player.getLocation().getBlockZ();
    	    	
    	if(games.getString("Games." + gameID + ".Lobby.world") == null)
    	{    	
	    	games.set("Games." + gameID + ".Lobby.world", world);
	    	games.set("Games." + gameID + ".Lobby.x", x);
	    	games.set("Games." + gameID + ".Lobby.y", y);
	    	games.set("Games." + gameID + ".Lobby.z", z);
	    	
	    	plugin.saveGamesConfig(games);
	    	games = null;
	    	
	    	player.sendMessage(StringUtils.color("&5The Lobby for game " + gameID + " has been set Successfully"));

    		player.sendMessage(StringUtils.color("&6Now please set the opposite red arena corners using /mobstrike setRedArena " + gameID));
    	}
    	else
    	{
    		player.sendMessage(StringUtils.color("&5The Lobby for game " + gameID + " has already been set"));
    	}
    }
}