package com.p376em.MobStrike.Commands.SubCommands.Edit;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Commands.CommandExecutor;
import com.p376em.MobStrike.Events.Entities.CustomMobs.MobManager;

public class testCommand extends CommandExecutor
{
	 
   public testCommand()
   {
       setCommand("test");
       setLength(1);
       setUsage("/mobstrike test");
       setBoth();
       setPermission("mobstrike.admin.create");
   }

   private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration settings = plugin.getSettingsConfig();	
	FileConfiguration shop = plugin.getShopConfig();
	FileConfiguration games = plugin.getGamesConfig();
	FileConfiguration gamedata = plugin.getGameDataConfig();
	int itemID = 5;
   MobManager mobman = new MobManager();
	
	
   @Override
   public void execute(CommandSender sender, String[] args)
   {   	
	   Player player = (Player) sender;
	   
	   int shopID = settings.getInt("Settings.Games.ShopID");
		
		String mobName = shop.getString("Shop." + shopID + ".Items." + itemID + ".Name"); 
		
		String playerTeam = gamedata.getString("GameData." + player.getUniqueId() + ".Team");
								
		if(playerTeam.equalsIgnoreCase("red"))
		{
			playerTeam = "blue";
		}
		else if(playerTeam.equalsIgnoreCase("blue"))
		{
			playerTeam = "red";
		}
		
		if(mobName.equalsIgnoreCase("guardian"))
		{
			mobman.spawnMob(mobman.spawnRandomiser("Water", mobName, playerTeam), player, itemID, mobName);
		}
		else if(mobName.equalsIgnoreCase("shulker"))
		{
			mobman.spawnMob(mobman.spawnRandomiser("Shulker", mobName, playerTeam), player, itemID, mobName);
		}
		else
		{
			System.out.println(mobman.spawnRandomiser("Land", mobName, playerTeam));
			
			mobman.spawnMob(mobman.spawnRandomiser("Land", mobName, playerTeam), player, itemID, mobName);
		}
   }
}