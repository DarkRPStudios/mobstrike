package com.p376em.MobStrike.Commands.SubCommands.Edit;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Commands.CommandExecutor;
import com.p376em.MobStrike.Utils.StringUtils;


public class bungeeCommand extends CommandExecutor
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration settings = null;
	 
   public bungeeCommand()
   {
       setCommand("setbungee");
       setLength(2);
       setUsage("/assassin setbungee <Enabled true / false>");
       setBoth();
       setPermission("assassin.admin.create");
   }

   @Override
   public void execute(CommandSender sender, String[] args)
   {   	
	   	Player player = (Player) sender;
	   	String enabled = args[1];
	   	settings = plugin.getSettingsConfig();
	   	
	   	settings.set("Settings.Bungeecord.Enabled", enabled);
	   	plugin.saveGamesConfig(settings);
	   	settings = null;
	   	
	   	if(enabled.equalsIgnoreCase("true"))
	   	{
	   		player.sendMessage(StringUtils.color("&2Bungeecord has been enabled"));
	   	}
	   	else if(enabled.equalsIgnoreCase("false"))
	   	{
	   		player.sendMessage(StringUtils.color("&4Bungeecord has been disabled"));
	   	}
   }
}