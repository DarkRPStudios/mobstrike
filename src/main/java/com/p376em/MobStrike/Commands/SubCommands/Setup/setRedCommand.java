package com.p376em.MobStrike.Commands.SubCommands.Setup;

import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Commands.CommandExecutor;
import com.p376em.MobStrike.Utils.StringUtils;


public class setRedCommand extends CommandExecutor
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration games = null;
    public setRedCommand()
    {
        setCommand("setredspawn");
        setLength(2);
        setUsage("/mobstrike setredspawn <GameID>");
        setPlayer();
        setPermission("mobstrike.admin.create");
    }
    
    @Override
    public void execute(CommandSender sender, String[] args)
    {
    	games = plugin.getGamesConfig();
    	
    	Player player = (Player) sender;
    	Integer gameID = Integer.valueOf(args[1]);
    	    	
    	String world = player.getLocation().getWorld().getName();
    	int x = player.getLocation().getBlockX();
    	int y = player.getLocation().getBlockY();
    	int z = player.getLocation().getBlockZ();
		int maxPlayers = games.getInt("Games." + gameID + ".Settings.MaxPlayers") / 2;
    	int i = 0;    
    	
    	for(i = 1; i <= maxPlayers; i++)
    	{
    		if(games.getString("Games." + gameID + ".PlayerSpawns.Red." + i + ".world") == null)
    		{
    			games.set("Games." + gameID + ".PlayerSpawns.Red." + i + ".world", world);
    			games.set("Games." + gameID + ".PlayerSpawns.Red." + i + ".x", x);
    			games.set("Games." + gameID + ".PlayerSpawns.Red." + i + ".y", y);
    			games.set("Games." + gameID + ".PlayerSpawns.Red." + i + ".z", z);
    	    	
    	    	plugin.saveGamesConfig(games);
    	    	games = null;
    	    	player.sendMessage(StringUtils.color("&5Red team spawn point &7(" + i + "/" + maxPlayers + ") &5for game " + gameID + " has been set Successfully"));
    	    	
    	    	if(i == maxPlayers)
    	    	{
    	    		player.sendMessage(StringUtils.color("&6Now please set the blue spawn points using /mobstrike setblue " + gameID));
    	    	}
    	    	return;
    		}
    	}
    }
}