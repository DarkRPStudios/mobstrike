package com.p376em.MobStrike;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.p376em.MobStrike.Commands.CommandHandler;
import com.p376em.MobStrike.Database.MySQL.MySQL;
import com.p376em.MobStrike.Events.Block.BlockBreak;
import com.p376em.MobStrike.Events.Block.BlockPlace;
import com.p376em.MobStrike.Events.Entities.EntityCombust;
import com.p376em.MobStrike.Events.Game.GameManager;
import com.p376em.MobStrike.Events.Game.GameState;
import com.p376em.MobStrike.Events.Game.GameStatus;
import com.p376em.MobStrike.Events.Inventory.InventoryClick;
import com.p376em.MobStrike.Events.Inventory.InventoryDrag;
import com.p376em.MobStrike.Events.Inventory.InventoryMoveItem;
import com.p376em.MobStrike.Events.Player.PlayerChat;
import com.p376em.MobStrike.Events.Player.PlayerDamage;
import com.p376em.MobStrike.Events.Player.PlayerDrop;
import com.p376em.MobStrike.Events.Player.PlayerInteract;
import com.p376em.MobStrike.Events.Player.PlayerInteractEntity;
import com.p376em.MobStrike.Events.Player.PlayerJoin;
import com.p376em.MobStrike.Events.Player.PlayerMove;
import com.p376em.MobStrike.Events.Player.PlayerQuit;
import com.p376em.MobStrike.Events.World.WeatherChange;
import com.p376em.MobStrike.Events.World.WorldRegenerator;
import com.p376em.MobStrike.Managers.EXP.EXPManager;
import com.p376em.MobStrike.Managers.Scoreboard.LobbyScoreboard;
import com.p376em.MobStrike.Utils.StringUtils;

import net.md_5.bungee.api.ChatColor;


public class MobStrike extends JavaPlugin
{
	  public static final String ANSI_RESET = "\033[0m";
	  public static final String ANSI_GREEN = "\033[32m";
	  public static final String ANSI_CYAN = "\033[36m";
	  
	  private static MobStrike instance;
	  
	  PluginDescriptionFile pluginyaml = getDescription();
	  
	  private MySQL createMySQL;
	  private WorldRegenerator worldRegen;
	  private GameManager gameManager;
	  private GameStatus gameStatus;
	  private EXPManager xpManager;
	  private LobbyScoreboard lobbyScoreboard = new LobbyScoreboard();
	  
	  private File settingsfile;
	  private File databasefile;
	  private File messagesfile;
	  private File gamesfile;
	  private File shopfile;
	  private File armouryfile;
	  private File gamedatafile;
	  
	  private FileConfiguration settingsconfig;
	  private FileConfiguration databaseconfig;
	  private FileConfiguration messagesconfig;
	  private FileConfiguration gamesconfig;
	  private FileConfiguration shopconfig;
	  private FileConfiguration armouryconfig;
	  private FileConfiguration gamedataconfig;

	  @Override
	public void onEnable()
	{
		instance = this;
		
	    createYAMLFiles();
		
	    registerEvents();
	    
	    getCommand("mobstrike").setExecutor(new CommandHandler());
	  
	    System.out.println("\033[32m------------- Mob Strike -------------\033[0m");
	    System.out.println("\033[32mStatus: Enabled\033[0m");
	    System.out.println("\033[32mDesigned By: Jaown21\033[0m");
	    System.out.println("\033[32mDeveloped By: P376EM\033[0m");
	    System.out.println("\033[32mVersion: " + pluginyaml.getVersion() + "\033[0m");
	    System.out.println("\033[32mWebsite: " + pluginyaml.getWebsite() + "\033[0m");
	    if(getDatabaseConfig().getBoolean("Database.Mysql.Enabled"))
	    {
	        setupMySQL();
	    }
	    else if(getDatabaseConfig().getBoolean("Database.Sql.Enabled"))
	    {
	    	
	    }
	    else if (getDatabaseConfig().getBoolean("Database.Sqlite.Enabled"))
	    {
	          
	    }
	    else
	    {
	    	Bukkit.getConsoleSender().sendMessage("\033[32mDatabase: " + StringUtils.color("&4No Database Connection Enabled \n Mob Strike will save to the YAML Files"));
	    }
	    
	    
	    //worldRegen.worldRegen();
	    runnables();
	    setGameStatus();
	    gameManager = new GameManager();
	}
	
	  @Override
	public void onDisable()
	{
		System.out.println("\033[36m------------- Mob Strike -------------\033[0m");
	    System.out.println("\033[36mStatus: Disabled\033[0m");
	    System.out.println("\033[36mDesigned By: Jaown21\033[0m");
	    System.out.println("\033[36mDeveloped By: P376EM\033[0m");
	    System.out.println("\033[36mVersion: " + pluginyaml.getVersion() + "\033[0m");
	    System.out.println("\033[36mWebsite: " + pluginyaml.getWebsite() + "\033[0m");
	    System.out.println("\033[36m--------------------------------------\033[0m");
	}
	
	public void setupMySQL()
	{
		createMySQL = new MySQL();
		createMySQL.databaseExists();
	}

	public static MobStrike getInstance()
	{
		return instance;
	}
	
			
	private void registerEvents()
	{
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new PlayerJoin(), this);
		pm.registerEvents(new PlayerQuit(), this);
		pm.registerEvents(new BlockBreak(), this);
		pm.registerEvents(new BlockPlace(), this);
		pm.registerEvents(new WeatherChange(), this);
		pm.registerEvents(new PlayerChat(), this);
		pm.registerEvents(new PlayerDamage(), this);
		pm.registerEvents(new PlayerMove(), this);
		pm.registerEvents(new PlayerInteract(), this);
		pm.registerEvents(new PlayerInteractEntity(), this);
		pm.registerEvents(new PlayerDrop(), this);
		pm.registerEvents(new InventoryClick(), this);
		pm.registerEvents(new InventoryDrag(), this);
		pm.registerEvents(new InventoryMoveItem(), this);
		pm.registerEvents(new EntityCombust(), this);
	}
	
	public void runnables()
	{
		lobbyScoreboard.runTaskTimer(this, 20, 20);
    }

	public void createYAMLFiles()
	{
		settingsfile = new File(getDataFolder(), "settings.yml");
		databasefile = new File(getDataFolder(), "database.yml");
		messagesfile = new File(getDataFolder(), "messages.yml");
		gamesfile = new File(getDataFolder(), "games.yml");
		shopfile = new File(getDataFolder(), "shop.yml");
		armouryfile = new File(getDataFolder(), "armoury.yml");
		gamedatafile = new File(getDataFolder(), "game_data.yml");
		
		if(!settingsfile.exists())
		{
			settingsfile.getParentFile().mkdirs();
			saveResource("settings.yml", false);
		}
		
		if(!databasefile.exists())
		{
			databasefile.getParentFile().mkdirs();
			saveResource("database.yml", false);
		}
		
		if(!messagesfile.exists())
		{
			messagesfile.getParentFile().mkdirs();
			saveResource("messages.yml", false);
		}
		
		if(!gamesfile.exists())
		{
			gamesfile.getParentFile().mkdirs();
			saveResource("games.yml", false);
		}
		
		if(!shopfile.exists())
		{
			shopfile.getParentFile().mkdirs();
			saveResource("shop.yml", false);
		}
		
		if(!armouryfile.exists())
		{
			armouryfile.getParentFile().mkdirs();
			saveResource("armoury.yml", false);
		}
		
		if(!gamedatafile.exists())
		{
			gamedatafile.getParentFile().mkdirs();
			saveResource("game_data.yml", false);
		}
		
		settingsconfig = YamlConfiguration.loadConfiguration(settingsfile);
		databaseconfig = YamlConfiguration.loadConfiguration(databasefile);
		messagesconfig = YamlConfiguration.loadConfiguration(messagesfile);
		gamesconfig = YamlConfiguration.loadConfiguration(gamesfile);
		shopconfig = YamlConfiguration.loadConfiguration(shopfile);
		armouryconfig = YamlConfiguration.loadConfiguration(armouryfile);
		gamedataconfig = YamlConfiguration.loadConfiguration(gamedatafile);
		
	}
	
	public FileConfiguration getSettingsConfig()
	{
		return settingsconfig = YamlConfiguration.loadConfiguration(settingsfile);
	}
	
	public FileConfiguration getDatabaseConfig()
	{
		return databaseconfig = YamlConfiguration.loadConfiguration(databasefile);
	}
	
	public FileConfiguration getMessagesConfig()
	{
		return messagesconfig = YamlConfiguration.loadConfiguration(messagesfile);
	}
	
	public FileConfiguration getGamesConfig()
	{
		return gamesconfig = YamlConfiguration.loadConfiguration(gamesfile);
	}
	
	public FileConfiguration getShopConfig()
	{
		return shopconfig = YamlConfiguration.loadConfiguration(shopfile);
	}
	
	public FileConfiguration getArmouryConfig()
	{
		return armouryconfig = YamlConfiguration.loadConfiguration(armouryfile);
	}
	
	public FileConfiguration getGameDataConfig()
	{
		return gamedataconfig = YamlConfiguration.loadConfiguration(gamedatafile);
	}
	
	public void saveSettingsConfig(FileConfiguration settingsconf)
	{
		try
		{
			settingsconf.save(settingsfile);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		settingsconfig = null;
		settingsconfig = YamlConfiguration.loadConfiguration(settingsfile);
		
	}
	
	public void saveDatabaseConfig()
	{
		try
		{
			databaseconfig.save(databasefile);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		databaseconfig = null;
		databaseconfig = YamlConfiguration.loadConfiguration(databasefile);
		
	}
	
	public void saveMessagesConfig()
	{
		try
		{
			messagesconfig.save(messagesfile);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		messagesconfig = null;
		messagesconfig = YamlConfiguration.loadConfiguration(messagesfile);
		
	}
	
	public void saveGamesConfig(FileConfiguration gamesconf)
	{
		try
		{
			gamesconf.save(gamesfile);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		gamesconfig = null;		
		gamesconfig = YamlConfiguration.loadConfiguration(gamesfile);
		
	}
	
	public void saveShopConfig(FileConfiguration shopconf)
	{
		try
		{
			shopconf.save(shopfile);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		shopconfig = null;		
		shopconfig = YamlConfiguration.loadConfiguration(shopfile);
		
	}
	
	public void saveArmouryConfig(FileConfiguration armouryconf)
	{
		try
		{
			armouryconf.save(armouryfile);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		armouryconfig = null;		
		armouryconfig = YamlConfiguration.loadConfiguration(armouryfile);
		
	}
	
	public void saveGameDataConfig(FileConfiguration gamedataconf)
	{
		try
		{
			gamedataconf.save(gamedatafile);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		gamedataconfig = null;		
		gamedataconfig = YamlConfiguration.loadConfiguration(gamedatafile);
		
	}

	public GameManager getGameManager()
	{
		return gameManager;
	}
	
	public EXPManager getEXPManager()
	{
		return xpManager;
	}
	
	public void setGameStatus()
	{
		GameStatus.setGameStatus(GameState.WAITING);
	}
}
