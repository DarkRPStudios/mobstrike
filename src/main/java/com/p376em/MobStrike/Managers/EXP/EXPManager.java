package com.p376em.MobStrike.Managers.EXP;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Events.Game.GameState;
import com.p376em.MobStrike.Events.Game.GameStatus;
import com.p376em.MobStrike.Utils.PacketUtils;
import com.p376em.MobStrike.Utils.StringUtils;
import com.p376em.MobStrike.Utils.playerConverter;


public class EXPManager
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	private FileConfiguration game = plugin.getGamesConfig();
	private FileConfiguration settings = plugin.getSettingsConfig();
	
	private int gameID = settings.getInt("Settings.Games.PlayGameID");
	private int maxXP = game.getInt("Games." + gameID + ".Settings.MaxEXP");
	private ArrayList<UUID> maxMessageList = new ArrayList<UUID>();
	
	public void setPlayerXP(UUID uuid, int xpPoints)
	{
		resetPlayerXP(uuid);
		Player player = playerConverter.uuidToPlayer(uuid);
		player.setLevel(xpPoints);		
		sendActionBar(uuid);
	}
	
	public void setPlayerTotalXP(UUID uuid, int xpPoints)
	{
		resetPlayerXP(uuid);
		Player player = playerConverter.uuidToPlayer(uuid);
		player.setLevel(xpPoints);		
		sendActionBar(uuid);
	}
	
	public void resetPlayerXP(UUID uuid)
	{
		Player player = playerConverter.uuidToPlayer(uuid);
		player.setTotalExperience(0);
		player.setLevel(0);
		sendActionBar(uuid);
	}
	
	public int getPlayerXP(UUID uuid)
	{
		return playerConverter.uuidToPlayer(uuid).getLevel();
	}
	
	public boolean exceedMaxEXP(UUID uuid, float exp)
	{
		if(exp > maxXP)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void addEXP(UUID uuid, int xp)
	{
		int exp = getPlayerXP(uuid) + xp;
		Player player = playerConverter.uuidToPlayer(uuid);
		
		if(GameStatus.getGameStatus().equals(GameState.INGAME))
		{
			if(exceedMaxEXP(uuid, exp))
			{
				player.setLevel(maxXP);
				
				if(!maxMessageList.contains(uuid))
				{
					maxMessageList.add(uuid);
				}

				sendActionBar(uuid);
			}
			else if(!exceedMaxEXP(uuid, exp))
			{
				player.setLevel(exp);		
				sendActionBar(uuid);
			}
		}
		else
		{
			return;
		}
	}
	
	public void sendActionBar(UUID uuid)
	{
		Player player = playerConverter.uuidToPlayer(uuid);
		
		String message = "EXP Points " + getPlayerXP(uuid) + " / " + maxXP;
		
		player.sendMessage(message);
		
	}
	
	public boolean payEXP(UUID uuid, int payment)
	{
		Player player = playerConverter.uuidToPlayer(uuid);
		
		int playerXP = getPlayerXP(uuid);
		if(payment <= playerXP)
		{
			int newBal = playerXP - payment;
			setPlayerTotalXP(uuid, newBal);
			player.sendMessage(StringUtils.color("&aTransaction Successful"));
			maxMessageList.remove(uuid);
			return true;
		}
		else if(payment > playerXP)
		{
			player.sendMessage(StringUtils.color("&4Transaction Failed"));
			return false;
		}
		else
		{
			player.sendMessage(StringUtils.color("&4Transaction Error"));
			return false;
		}
	}
}