package com.p376em.MobStrike.Managers.Scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.p376em.MobStrike.Utils.StringUtils;
 
public class LobbyScoreboard extends BukkitRunnable
{
    @Override
    public void run()
    {
	    for(Player player : Bukkit.getOnlinePlayers())
	    {
	    	ScoreboardManager board_manager = new ScoreboardManager(StringUtils.color("&1MobStrike"));
		    board_manager.addBlankSpace();
		    board_manager.addLine(StringUtils.color("&2Voting Status:"));
		    board_manager.addLine(StringUtils.color("&dNot Voted"));
			board_manager.addBlankSpace();
			//board_manager.addLine(StringUtils.color("&aGame Starts in: " + count.getRemainingTime()));
			board_manager.addBlankSpace();
			board_manager.addLine(ChatColor.GOLD+""+ChatColor.BOLD+"play.helixcraft.net");
			player.setScoreboard(board_manager.getScoreboard());
	    }	    
    } 
}