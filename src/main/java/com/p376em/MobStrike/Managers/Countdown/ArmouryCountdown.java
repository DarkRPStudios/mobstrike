package com.p376em.MobStrike.Managers.Countdown;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Utils.StringUtils;

public class ArmouryCountdown extends BukkitRunnable
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	private FileConfiguration settings = plugin.getSettingsConfig();
	int countdown = settings.getInt("Settings.Games.ArmouryCountdown");
	
			@Override
			public void run()
			{
				if(countdown % 15 == 0 && countdown <= 60 && countdown > 10)
		       	{
		      		Bukkit.broadcastMessage(StringUtils.color("&2" + countdown + " seconds remaining in the armoury"));
		       	}
		       	else if(countdown <= 10 && countdown > 5)
		       	{
		      		Bukkit.broadcastMessage(StringUtils.color("&2" + countdown + " seconds remaining in the armoury"));
		      	}
		       	else if(countdown <= 5 && countdown > 1)
		       	{
		       		Bukkit.broadcastMessage(StringUtils.color("&2" + countdown + " seconds remaining in the armoury"));
		           		
		       		for (Player online : Bukkit.getOnlinePlayers())
		       		{
		       			online.playSound(online.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 2, 2);
		            }
		       	}
		       	else if(countdown == 1)
		       	{
		       		Bukkit.broadcastMessage(StringUtils.color("&2" + countdown + " second remaining in the armoury"));
		           		
		       		for (Player online : Bukkit.getOnlinePlayers())
		       		{
		       			online.playSound(online.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 2, 2);
		            }
		       	}
		      	else if(countdown == 0)
		      	{
		      		plugin.getGameManager().arenaStart();
		       		this.cancel();
		        }
				
				countdown--;
			}
		
}