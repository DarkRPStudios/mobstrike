package com.p376em.MobStrike.Managers.Countdown;

import org.bukkit.scheduler.BukkitRunnable;

import com.p376em.MobStrike.MobStrike;

public class MobLevelsTimer extends BukkitRunnable
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	
	@Override
	public void run()
	{
		plugin.getGameManager().givePlayersEXP();
	}
		
}