package com.p376em.MobStrike.Managers.Countdown;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Utils.StringUtils;

public class LobbyCountdown extends BukkitRunnable
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	private FileConfiguration settings = plugin.getSettingsConfig();
	int countdown = settings.getInt("Settings.Games.LobbyCountdown");
	
	private boolean isRunning = false; 
	
	public boolean isRunning()
	{
		return isRunning;
	}

	public void setRunning(boolean isRunning)
	{
		this.isRunning = isRunning;
	}

			@Override
			public void run()
			{
				if(countdown % 15 == 0 && countdown <= 60 && countdown > 10)
		       	{
		      		Bukkit.broadcastMessage(StringUtils.color("&2The game will start in " + countdown + " seconds"));
		       	}
		       	else if(countdown <= 10 && countdown > 5)
		       	{
		      		Bukkit.broadcastMessage(StringUtils.color("&2The game will start in " + countdown + " seconds"));
		      	}
		       	else if(countdown <= 5 && countdown > 1)
		       	{
		       		Bukkit.broadcastMessage(StringUtils.color("&2The game will start in " + countdown + " seconds"));
		           		
		       		for (Player online : Bukkit.getOnlinePlayers())
		       		{
		       			online.playSound(online.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 2, 2);
		            }
		       	}
		       	else if(countdown == 1)
		       	{
		       		Bukkit.broadcastMessage(StringUtils.color("&2The game will start in " + countdown + " second"));
		           		
		       		for (Player online : Bukkit.getOnlinePlayers())
		       		{
		       			online.playSound(online.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 2, 2);
		            }
		       	}
		      	else if(countdown == 0)
		      	{
		      		Bukkit.broadcastMessage(StringUtils.color("&eThe game is starting\nHave fun :D"));
	          		
		       		for (Player online : Bukkit.getOnlinePlayers())
		       		{
		       			online.playSound(online.getLocation(), Sound.ENTITY_CREEPER_PRIMED, 2, 2);
		            }
		       		isRunning = false;
		       		plugin.getGameManager().gameStart();
		       		this.cancel();
		        }
				
				countdown--;
			}
		
}