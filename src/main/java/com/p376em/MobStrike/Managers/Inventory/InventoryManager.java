package com.p376em.MobStrike.Managers.Inventory;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Utils.StringUtils;

public class InventoryManager
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration settings = plugin.getSettingsConfig();	
	FileConfiguration shop = plugin.getShopConfig();
	FileConfiguration armoury = plugin.getArmouryConfig();
	
	public void shopInventory(Player player)
	{
			int shopID = settings.getInt("Settings.Games.ShopID");
			if(shop.getString("Shop." + shopID + ".Settings.ShopID") == null) {return;}
			int shopSlots = shop.getInt("Shop." + shopID + ".Settings.inventorySlots");
			
			String shopName = StringUtils.color(shop.getString("Shop." + shopID + ".Settings.shopName"));
			
			int i = 0;
			
			if(shop.getConfigurationSection("Shop." + shopID + ".Items") == null)
			{
				player.sendMessage(StringUtils.color("&4There are no items in the shop config file\nPlease use /mobstrike addshopitem"));
				return;
			}
				
			int shopSize = shop.getConfigurationSection("Shop." + shopID + ".Items").getKeys(false).size();
			
			Inventory inventory = Bukkit.getServer().createInventory(null, shopSlots, shopName);
			
			for(i = 1; i <= shopSize; i++)
			{
				ItemStack item;
				
				String itemBlock = shop.getString("Shop." + shopID + ".Items." + i + ".Item").toUpperCase();
				String itemEffect = shop.getString("Shop." + shopID + ".Items." + i + ".Effect");
				String itemName = shop.getString("Shop." + shopID + ".Items." + i + ".Name");
				String[] itemArray = itemBlock.split(":");
				String itemPrice = shop.getString("Shop." + shopID + ".Items." + i + ".Price");
				String itemEXP = shop.getString("Shop." + shopID + ".Items." + i + ".EXPLevels");
				String itemCooldown  = shop.getString("Shop." + shopID + ".Items." + i + ".Cooldown");
				
				Material mat = Material.valueOf(itemArray[0].toUpperCase());
				
				int itemSlot = shop.getInt("Shop." + shopID + ".Items." + i + ".ItemSlot");
								
				if(itemArray.length == 1)
				{
					item = new ItemStack(mat);
				}
				else
				{
					Bukkit.getConsoleSender().sendMessage("Error with the material name and failed to process!\nErroring Item: " + mat + "\nShop Item: " + i);
					return;
				}
							
				ItemMeta itemMeta = item.getItemMeta();
					
					ArrayList<String> itemLore = new ArrayList<String>();
					
					itemLore.add(itemName);
					itemLore.add(itemCooldown);
					itemLore.add(itemEXP);
					itemLore.add(itemPrice);
					itemLore.add(itemBlock);
					itemLore.add(itemEffect);
					
					itemMeta.setDisplayName(itemName);
					itemMeta.setLore(itemLore);
					
					item.setItemMeta(itemMeta);
				
				
				inventory.setItem(itemSlot, item);
				
				player.openInventory(inventory);
		}
	}
	
	public void armouryInventory(Player player)
	{
			int armouryID = settings.getInt("Settings.Games.ArmouryID");
			if(armoury.getString("Armoury." + armouryID + ".Settings.armouryID") == null) {return;}
			int armourySlots = armoury.getInt("Armoury." + armouryID + ".Settings.inventorySlots");
			
			String armouryName = StringUtils.color(armoury.getString("Armoury." + armouryID + ".Settings.armouryName"));
			
			int i = 0;
			
			if(armoury.getConfigurationSection("Armoury." + armouryID + ".Items") == null)
			{
				player.sendMessage(StringUtils.color("&4Their are no items in the armoury config file\nPlease use /mobstrike addarmouryitem"));
				return;
			}
			
			int armourySize = armoury.getConfigurationSection("Armoury." + armouryID + ".Items").getKeys(false).size();
			
			Inventory inventory = Bukkit.getServer().createInventory(null, armourySlots, armouryName);
			
			for(i = 1; i <= armourySize;i++)
			{
				ItemStack item;
				
				String itemBlock = armoury.getString("Armoury." + armouryID + ".Items." + i + ".Item");
				String itemEffect = armoury.getString("Armoury." + armouryID + ".Items." + i + ".Effect");
				String itemName = armoury.getString("Armoury." + armouryID + ".Items." + i + ".Name");
				String[] itemArray = itemBlock.split(":");
				String itemPrice = armoury.getString("Armoury." + armouryID + ".Items." + i + ".Price");
				String itemEXP = armoury.getString("Armoury." + armouryID + ".Items." + i + ".EXPLevels");
				String itemCooldown  = armoury.getString("Armoury." + armouryID + ".Items." + i + ".Cooldown");
				
				Material mat = Material.valueOf(itemArray[0].toUpperCase());
				
				int itemSlot = armoury.getInt("Armoury." + armouryID + ".Items." + i + ".ItemSlot");
				
				if(itemArray.length == 1)
				{
					item = new ItemStack(mat);
				}
				else if(itemArray.length == 2)
				{					
					int color = Integer.valueOf(itemArray[1]);
					item = new ItemStack(mat, 1, (byte) color);
				}
				else
				{
					Bukkit.getConsoleSender().sendMessage("Error with the material name and failed to process!");
					return;
				}
							
				ItemMeta itemMeta = item.getItemMeta();
				
				ArrayList<String> itemLore = new ArrayList<String>();
				
				itemLore.add(itemName);
				itemLore.add(itemCooldown);
				itemLore.add(itemEXP);
				itemLore.add(itemPrice);
				itemLore.add(itemBlock);
				itemLore.add(itemEffect);
				
				itemMeta.setDisplayName(itemName);
				itemMeta.setLore(itemLore);
				
				item.setItemMeta(itemMeta);
				
				inventory.setItem(itemSlot, item);
				
				player.openInventory(inventory);
		}
	}
	
	public void votingInventory(Player player)
	{
		player.getInventory().clear();
		
		ItemStack redConcrete = new ItemStack(Material.RED_CONCRETE);
		ItemStack blueConcrete = new ItemStack(Material.BLUE_CONCRETE);
		
		ItemMeta redItemMeta = redConcrete.getItemMeta();
		ItemMeta blueItemMeta = blueConcrete.getItemMeta();
		
		redItemMeta.setDisplayName(StringUtils.color("&4Vote To Join Red Team"));
		blueItemMeta.setDisplayName(StringUtils.color("&1Vote To Join Blue Team"));
		
		redConcrete.setItemMeta(redItemMeta);
		blueConcrete.setItemMeta(blueItemMeta);
		
		player.getInventory().setItem(2, redConcrete);
		player.getInventory().setItem(6, blueConcrete);
	}
}
