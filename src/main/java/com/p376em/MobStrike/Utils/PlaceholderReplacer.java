package com.p376em.MobStrike.Utils;

public class PlaceholderReplacer
{
	public static String replaceValues(String oldString)
	{
		String newString;
		
		for(PlaceholderEnum placeholder : PlaceholderEnum.values())
		{
			String holder = "%" + placeholder + "%";
			
			if(oldString.contains(holder))
			{
				newString = oldString.replaceAll(holder, "");
				return newString;
			}
		}
		
		return oldString;
		
	}
}
