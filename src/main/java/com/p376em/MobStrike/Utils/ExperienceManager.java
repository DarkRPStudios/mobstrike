package com.p376em.MobStrike.Utils;

import org.bukkit.entity.Player;

public class ExperienceManager
{
	public void setEXP(Player player, int value)
	{
		player.setLevel(value);
	}
	
	public int getEXP(Player player)
	{
		return player.getLevel();
	}
	
	public boolean payEXPWithCallback(Player player, int value)
	{
		int bal = getEXP(player);
		
		if(bal >= value)
		{
			int newEXP = bal - value;
			setEXP(player, newEXP);
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void depositEXP(Player player, int value)
	{
		int exp = getEXP(player);
		int setExp = exp + value;
		setEXP(player, setExp);
	}
}
