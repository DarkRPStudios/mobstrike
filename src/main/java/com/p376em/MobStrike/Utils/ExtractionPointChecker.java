package com.p376em.MobStrike.Utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

import com.p376em.MobStrike.MobStrike;


public class ExtractionPointChecker
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration games = plugin.getGamesConfig();
	
	public boolean playerInsideExtractionPoint(Location playerLocation)
	{
		int gameID = plugin.getSettingsConfig().getInt("Settings.plugin.getGamesConfig().PlayGameID");
		
		World world1 = Bukkit.getServer().getWorld(plugin.getGamesConfig().getString("Games." + gameID + ".extraction.1.world"));
		double x1 = games.getDouble("Games." + gameID + ".extraction.1.x");
		double y1 = games.getDouble("Games." + gameID + ".extraction.1.y");
		double z1 = games.getDouble("Games." + gameID + ".extraction.1.z");
		
		World world2 = Bukkit.getServer().getWorld(plugin.getGamesConfig().getString("Games." + gameID + ".extraction.2.world"));
		double x2 = games.getDouble("Games." + gameID + ".extraction.2.x");
		double y2 = games.getDouble("Games." + gameID + ".extraction.2.y");
		double z2 = games.getDouble("Games." + gameID + ".extraction.2.z");
				
		Location ext1 = new Location(world1, x1, y1, z1);  
		Location ext2 = new Location(world2, x2, y2, z2); 
		
		String ext1World = ext1.getWorld().getName();
		int ext1X = ext1.getBlockX();
		int ext1Y = ext1.getBlockY();
		int ext1Z = ext1.getBlockZ();
		
		String ext2World = ext2.getWorld().getName();
		int ext2X = ext2.getBlockX();
		int ext2Y = ext2.getBlockY();
		int ext2Z = ext2.getBlockZ();
		
		String playerWorld = playerLocation.getWorld().getName();
		int playerX = playerLocation.getBlockX();
		int playerY = playerLocation.getBlockY();
		int playerZ = playerLocation.getBlockZ();
		
		if(ext1World.equalsIgnoreCase(ext2World) && ext2World.equalsIgnoreCase(playerWorld))
		{
			if(ext1X < ext2X)
			{
				if(ext1X <= playerX && playerX <= ext2X)
				{
					if(ext1Y < ext2Y)
					{
						if(ext1Y <= playerY && playerY <= ext2Y)
						{
							if(ext1Z < ext2Z)
							{
								if(ext1Z <= playerZ && playerX <= ext2Z)
								{
									return true;
								}
								else
								{
									return false;
								}
							}
							else if(ext1Z > ext2Z)
							{
								if(ext1Z >= playerZ && playerZ >= ext2Z)
								{
									return true;
								}
								else
								{
									return false;
								}
							}
						}
						else
						{
							return false;
						}
					}
					else if(ext1Y > ext2Y)
					{
						if(ext1Y >= playerY && playerY >= ext2Y)
						{
							if(ext1Z < ext2Z)
							{
								if(ext1Z <= playerZ && playerX <= ext2Z)
								{
									return true;
								}
								else
								{
									return false;
								}
							}
							else if(ext1Z > ext2Z)
							{
								if(ext1Z >= playerZ && playerZ >= ext2Z)
								{
									return true;
								}
								else
								{
									return false;
								}
							}
						}
						else
						{
							return false;
						}
					}
				}
				else
				{
					return false;
				}
			}
			else if(ext1X > ext2X)
			{
				if(ext1X >= playerX && playerX >= ext2X)
				{
					if(ext1Y < ext2Y)
					{
						if(ext1Y <= playerY && playerY <= ext2Y)
						{
							if(ext1Z < ext2Z)
							{
								if(ext1Z <= playerZ && playerX <= ext2Z)
								{
									return true;
								}
								else
								{
									return false;
								}
							}
							else if(ext1Z > ext2Z)
							{
								if(ext1Z >= playerZ && playerZ >= ext2Z)
								{
									return true;
								}
								else
								{
									return false;
								}
							}
						}
						else
						{
							return false;
						}
					}
					else if(ext1Y > ext2Y)
					{
						if(ext1Y >= playerY && playerY >= ext2Y)
						{
							if(ext1Z < ext2Z)
							{
								if(ext1Z <= playerZ && playerX <= ext2Z)
								{
									return true;
								}
								else
								{
									return false;
								}
							}
							else if(ext1Z > ext2Z)
							{
								if(ext1Z >= playerZ && playerZ >= ext2Z)
								{
									return true;
								}
								else
								{
									return false;
								}
							}
						}
						else
						{
							return false;
						}
					}
				}
				else
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}	
		
		return false;
	}
}
