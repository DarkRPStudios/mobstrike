package com.p376em.MobStrike.Utils;

import org.bukkit.ChatColor;

public class StringUtils
{
  public static String color(String msg)
  {
    return ChatColor.translateAlternateColorCodes('&', msg);
  }
}
