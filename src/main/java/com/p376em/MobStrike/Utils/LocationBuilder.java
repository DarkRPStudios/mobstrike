package com.p376em.MobStrike.Utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

public class LocationBuilder
{
	public static Location basicLocation(String worldName, double x, double y, double z)
	{
		World world = Bukkit.getServer().getWorld(worldName);
		
		Location loc = new Location(world, x, y, z);
		
		return loc;
	}
	
	public static Location advancedLocation(String worldName, double x, double y, double z, float yaw, float pitch)
	{
		World world = Bukkit.getServer().getWorld(worldName);
		
		Location loc = new Location(world, x, y, z, yaw, pitch);
		
		return loc;
	}
}
