package com.p376em.MobStrike.Events.Game;

public enum GameState
{	
	WAITING,
	STARTING,
	INSHOP,
	INGAME,
	ROUNDEND,
	GAMEEND,
	RESTARTING;
}
