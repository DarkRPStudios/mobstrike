package com.p376em.MobStrike.Events.Game;

import org.bukkit.Bukkit;

public class GameStatus
{
	private static GameState gameState;
	
	public static GameState getGameStatus()
	{
		return gameState;
	}

	public static void setGameStatus(GameState newState)
	{
		gameState = newState;
		Bukkit.getConsoleSender().sendMessage(gameState.toString());
	}
}
