package com.p376em.MobStrike.Events.Game;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Events.Entities.CustomMobs.NPCManager;
import com.p376em.MobStrike.Managers.Countdown.ArmouryCountdown;
import com.p376em.MobStrike.Managers.Countdown.LobbyCountdown;
import com.p376em.MobStrike.Managers.Countdown.MobLevelsTimer;
import com.p376em.MobStrike.Managers.EXP.EXPManager;
import com.p376em.MobStrike.Utils.LocationBuilder;
import com.p376em.MobStrike.Utils.StringUtils;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class GameManager 
{
	private HashMap<UUID, Location> playerSpawnPoints = new HashMap<UUID, Location>();
	private ArrayList<UUID> players = new ArrayList<UUID>();
	private ArrayList<Location> Armoury = new ArrayList<Location>();
	private HashMap<String, Location> gameSpawns = new HashMap<String, Location>();
	private HashMap<String, Integer> roundWins = new HashMap<String, Integer>();
	private HashMap<UUID, String> playerTeams = new HashMap<UUID, String>();
	
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration settings = plugin.getSettingsConfig();
	FileConfiguration games = plugin.getGamesConfig();
	FileConfiguration shop = plugin.getShopConfig();
	FileConfiguration gamedata = null;
	private NPCManager npcManager = new NPCManager();
	private LobbyCountdown lobbyCountdown = new LobbyCountdown();
	private ArmouryCountdown armouryCountdown = new ArmouryCountdown();
	private VoteRegister voteCollector = new VoteRegister();
	private MobLevelsTimer mobLevels = new MobLevelsTimer();
	private EXPManager expManager = new EXPManager();
	
	int gameID = settings.getInt("Settings.Games.PlayGameID");
	int minPlayers = games.getInt("Games." + gameID + ".Settings.MaxPlayers");
		
	public void serverBoot()
	{
		
	}
	
	public void gameLoadSpawns()
	{
		int spawnsSetRed = games.getConfigurationSection("Games." + gameID + ".PlayerSpawns.Red").getKeys(false).size();
		int spawnsSetBlue = games.getConfigurationSection("Games." + gameID + ".PlayerSpawns.Blue").getKeys(false).size();
		int spawnsSetArmoury = games.getConfigurationSection("Games." + gameID + ".NPC.Armoury").getKeys(false).size();
    	int i = 0;    
    	String world;
    	double x;
    	double y;
    	double z;
    	Location spawnLoc;
    	
    	for(i = 1; i <= spawnsSetRed; i++)
    	{
    		if(games.getString("Games." + gameID + ".PlayerSpawns.Red." + i + ".world") == null || games.getString("Games." + gameID + ".PlayerSpawns.Blue." + i + ".world") == null)
        	{	
    			return;
        	}	
    		
    		world = games.getString("Games." + gameID + ".PlayerSpawns.Red." + i + ".world");
    		x = games.getDouble("Games." + gameID + ".PlayerSpawns.Red." + i + ".x");
    		y = games.getDouble("Games." + gameID + ".PlayerSpawns.Red." + i + ".y");
    		z = games.getDouble("Games." + gameID + ".PlayerSpawns.Red." + i + ".z");
    			
    		spawnLoc = LocationBuilder.basicLocation(world, x, y, z);
    		setSpawns("red", spawnLoc);
    		
    		world = null;
    		x = 0;
    		y = 0;
    		z = 0;
    		spawnLoc = null;
    	}
    	
    	for(i = 1; i <= spawnsSetBlue; i++)
    	{ 
    			
    		world = games.getString("Games." + gameID + ".PlayerSpawns.Blue." + i + ".world");
    		x = games.getDouble("Games." + gameID + ".PlayerSpawns.Blue." + i + ".x");
    		y = games.getDouble("Games." + gameID + ".PlayerSpawns.Blue." + i + ".y");
    		z = games.getDouble("Games." + gameID + ".PlayerSpawns.Blue." + i + ".z");
    		
    		spawnLoc = LocationBuilder.basicLocation(world, x, y, z);
    		setSpawns("blue", spawnLoc);
    			
    		world = null;
    		x = 0;
    		y = 0;
    		z = 0;
    		spawnLoc = null;
    	}
    	
    	for(i = 1; i <= spawnsSetArmoury; i++)
    	{ 
    			
    		world = games.getString("Games." + gameID + ".Armoury." + i + ".world");
    		x = games.getDouble("Games." + gameID + ".Armoury." + i + ".x");
    		y = games.getDouble("Games." + gameID + ".Armoury." + i + ".y");
    		z = games.getDouble("Games." + gameID + ".Armoury." + i + ".z");
    		
    		spawnLoc = LocationBuilder.basicLocation(world, x, y, z);
    		setArmoury(spawnLoc);
    			
    		world = null;
    		x = 0;
    		y = 0;
    		z = 0;
    		spawnLoc = null;
    	}
    		
    }

	public void gameStart()
	{
		for (Player online : Bukkit.getOnlinePlayers())
   		{
   			online.sendMessage(StringUtils.color("&6Team voting has now closed"));
        }
		
		gameLoadSpawns();
		
		npcManager.shopKeepers();
		npcManager.blackSmiths();
		
		int i = 1;
		
		for (Player player : Bukkit.getOnlinePlayers())
   		{
   			player.getInventory().clear();
   			player.updateInventory();
   			
   			if(i % 2 == 0)
   			{
   				setTeams(player.getUniqueId(), "Red");
   			}
   			else if(i % 2 == 1)
   			{
   				setTeams(player.getUniqueId(), "Blue");
   			}
   			
   			i++;
        }
		
		GameStatus.setGameStatus(GameState.INSHOP);
		
		Bukkit.broadcastMessage(StringUtils.color("&1The game has Started"));
		
		roundStart();
	}

	private void roundStart()
	{
		int spawnSetArmoury = games.getConfigurationSection("Games." + gameID + ".NPC.Armoury").getKeys(false).size();
		
		int spawnSet = 0;
			
		for (Player player : Bukkit.getOnlinePlayers())
		{
			removeBroughtItems();	
			player.setExp(0);
				
			if(spawnSet > spawnSetArmoury)
			{
				return;
			}
			
				player.teleport(getArmoury().get(spawnSet));
				spawnSet++;
	    }
		
		// missing setspawn loop throws index out of bounds error
		
		
		armouryCountdown.runTaskTimer(plugin, 20, 20);
		
	}
	
	public void arenaStart()
	{
		Bukkit.broadcastMessage(StringUtils.color("&4Let The Fight Begin!"));
		GameStatus.setGameStatus(GameState.INGAME);
		gameSpawnSetter();
	}
	
	public void playersRemaining()
	{
		int red = 0;
		int blue = 0;
		
		for (Player player : Bukkit.getOnlinePlayers())
		{
			if(player.getGameMode().equals(GameMode.SURVIVAL))
			{
				String team = playerTeams.get(player.getUniqueId());
				
				if(team == "Red")
				{
					red++;
				}
				else if(team == "Blue")
				{
					blue++;
				}
			}
		}
		
		if(red == 0)
		{
			roundEnd("blue");
		}
		else if(blue == 0)
		{
			roundEnd("red");
		}
	}

	public void roundEnd(String winningTeam)
	{
			
		GameStatus.setGameStatus(GameState.ROUNDEND);
		
		for (Player allPlayer : Bukkit.getOnlinePlayers())
        {
            allPlayer.sendMessage(StringUtils.color("&e" + winningTeam + " has Won!"));
        }
		
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "kill @e[type=!player]");
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "kill @e[type=Shulker]");
		
		
		gameEnd();
	}

	public void gameEnd()
	{
		if(plugin.getSettingsConfig().getBoolean("Settings.Bungeecord.Enabled") == true)
        {
			moveServer();
		}
		else if(plugin.getSettingsConfig().getBoolean("Settings.Bungeecord.Enabled") == false)
		{
			
		}
	}
	 
	public void moveServer()
	{
		 if (playerTeams.size() > 0)
		 {
             for(Player player : Bukkit.getOnlinePlayers())
             {
            	 ByteArrayOutputStream data = new ByteArrayOutputStream();
                 DataOutputStream out = new DataOutputStream(data);
                 try
                 {
                     out.writeUTF("Connect");
                     out.writeUTF(plugin.getSettingsConfig().getString("Settings.Bungeecord.GameEndLobby"));
                     player.sendPluginMessage(plugin, "BungeeCord", data.toByteArray());
                     out.close();
                     data.close();
                 }
                 catch (Exception ex)
                 {
                     ex.printStackTrace();
                 }
             }
			
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "restart");
		 }
		}
	
	public void removeBroughtItems()
	{		
		for(Player player : Bukkit.getOnlinePlayers())
		{
            UUID uuid = player.getUniqueId();
            
            gamedata = plugin.getGameDataConfig();
            
            gamedata.set("GameData." + uuid + ".Inventory", null);
            
            plugin.saveGameDataConfig(gamedata);
            gamedata = null;
            
            gamedata = plugin.getGameDataConfig();
            
            gamedata.createSection("GameData." + player.getUniqueId() + ".Inventory");
            
            plugin.saveGameDataConfig(gamedata);
            gamedata = null;
		}
	}
	
	public boolean playerCheck()
	{
		int playersOnline = getPlayers().size();
		if(playersOnline >= minPlayers)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private void gameSpawnSetter()
	{
		for(Player player : Bukkit.getOnlinePlayers())
		{
			gamedata = plugin.getGameDataConfig();
			String playerTeamColor = gamedata.getString("GameData." + player.getUniqueId()+ ".Team").toLowerCase();
			Location playerLoc = getSpawns().get(playerTeamColor);
			setPlayerTeams(player.getUniqueId(), playerTeamColor);
			setPlayerSpawnPoints(player.getUniqueId(), playerLoc);
			player.teleport(playerLoc);
		}
		resetPlayersEXP();
		mobLevels.runTaskTimer(plugin, 200, 80);
	}

	public void givePlayersEXP()
	{
		for(Player player : Bukkit.getOnlinePlayers())
		{
			if(player.getGameMode().equals(GameMode.SURVIVAL))
			{
				expManager.addEXP(player.getUniqueId(), 5);
			}
		}
	}
	
	public void resetPlayersEXP()
	{
		for(Player player : Bukkit.getOnlinePlayers())
		{
			expManager.resetPlayerXP(player.getUniqueId());
		}
	}
	
	public void setTeams(UUID uuid, String team)
	{
		gamedata = plugin.getGameDataConfig();
		gamedata.set("GameData." + uuid + ".Team", team);
		plugin.saveGameDataConfig(gamedata);
		gamedata = null;
	}

	public HashMap<UUID, Location> getPlayerSpawnPoints()
	{
		return playerSpawnPoints;
	}

	public void setPlayerSpawnPoints(UUID uuid, Location loc)
	{
		playerSpawnPoints.put(uuid, loc);
	}

	public ArrayList<UUID> getPlayers()
	{
		return players;
	}

	public void setPlayers(UUID uuid)
	{
		players.add(uuid);
		expManager.resetPlayerXP(uuid);
		
		if(settings.getBoolean("Settings.Games.SingleDevTesting") == true)
		{
			if(!lobbyCountdown.isRunning())
			{
				lobbyCountdown.runTaskTimer(plugin, 20, 20);
			}
			GameStatus.setGameStatus(GameState.STARTING);
		}
		else
		{
			PlayerListener();
		}
	}

	private void PlayerListener()
	{
		
		if(getPlayers().size() >= games.getInt("Games." + gameID + ".Settings.MinPlayers"))
		{
			if(!lobbyCountdown.isRunning())
			{
				lobbyCountdown.runTaskTimer(plugin, 20, 20);
			}
			GameStatus.setGameStatus(GameState.STARTING);
		}
	}

	public HashMap<UUID, String> getPlayerTeams()
	{
		return playerTeams;
	}

	public void setPlayerTeams(UUID uuid, String team)
	{
		playerTeams.put(uuid, team);
	}

	public HashMap<String, Location> getSpawns()
	{
		return gameSpawns;
	}

	public void setSpawns(String team, Location spawnLoc)
	{
		gameSpawns.put(team, spawnLoc);
	}

	public HashMap<String, Integer> getRoundWins()
	{
		return roundWins;
	}

	public void setRoundWins(String team, Integer score)
	{
		roundWins.put(team, score);
	}

	public void updateRoundWins(String team, Integer score)
	{
		roundWins.replace(team, score);
	}

	public ArrayList<Location> getArmoury()
	{
		return Armoury;
	}

	public void setArmoury(Location loc)
	{
		Armoury.add(loc);
	}

	public void removePlayers(UUID key)
	{
		players.remove(key);
	}
	
	public void removePlayerTeam(UUID key)
	{
		playerTeams.remove(key);
	}
	
	public void removePlayerSpawnPoints(UUID key)
	{
		playerSpawnPoints.remove(key);
	}
}