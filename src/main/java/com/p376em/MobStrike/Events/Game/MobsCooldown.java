package com.p376em.MobStrike.Events.Game;

import java.util.HashMap;

public class MobsCooldown
{
	private HashMap<String, Long> cooldownMap = new HashMap<String, Long>();

	public HashMap<String, Long> getCooldownMap()
	{
		return cooldownMap;
	}
	
	public Long getCooldown(String uuidAndBlock)
	{
		return cooldownMap.get(uuidAndBlock);
	}

	public void setCooldownMap(String uuidAndBlock, Long time)
	{
		cooldownMap.put(uuidAndBlock, time);
	}
	
	public void removeKey(String uuidAndBlock)
	{
		cooldownMap.remove(uuidAndBlock);
	}

	public void updateCooldownMap(String uuidAndBlock, Long time)
	{
		cooldownMap.remove(uuidAndBlock);
		cooldownMap.put(uuidAndBlock, time);
	}
}
