package com.p376em.MobStrike.Events.Game;

import java.util.HashMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.bukkit.entity.Player;

import com.p376em.MobStrike.Utils.StringUtils;

public class VoteRegister
{
	private HashMap <UUID, String> playerVoted = new HashMap<UUID, String>();
	private GameStatus gameStatus = new GameStatus();
	
	public void voteRegister(Player player, String vote)
	{
		UUID uuid = player.getUniqueId();
		
		if(gameStatus.getGameStatus().equals(GameState.WAITING) || gameStatus.getGameStatus().equals(GameState.STARTING))
		{
			for(Entry<UUID, String> playerVotes : getPlayerVoted().entrySet())
			{
				if(playerVotes.getKey() == uuid)
				{
					removePlayerVoted(uuid);
				}
			}
			
			setPlayerVoted(uuid, vote);
			
			player.sendMessage(StringUtils.color("&aYour Vote to join the " + vote + " team has been registered successfully"));
		}
		else
		{
			return;
		}
	}
	
	public HashMap<UUID, String> getPlayerVoted()
	{
		return playerVoted;
	}

	public void setPlayerVoted(UUID uuid, String voted)
	{
		playerVoted.put(uuid, voted);
	}
	
	public void removePlayerVoted(UUID uuid)
	{
		playerVoted.remove(uuid);
	}

	
}
