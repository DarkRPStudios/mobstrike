package com.p376em.MobStrike.Events.Player;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Events.Game.GameManager;

public class PlayerDamage implements Listener
{
	private GameManager gameManager = new GameManager();
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration settings = plugin.getSettingsConfig();
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void playerDamage(EntityDamageByEntityEvent e)
	{
		Entity entity = e.getEntity();
		Entity damager = e.getDamager();
		
		if(e.getCause().equals(DamageCause.LAVA) && entity.equals(EntityType.PLAYER))
		{
			e.setCancelled(true);
			entity.setFireTicks(0);
		}
		
		if(entity instanceof Player && damager instanceof Player)
		{			
			boolean friendlyFire = settings.getBoolean("Settings.Games.FriendlyFire");
			
			if(!friendlyFire)
			{
				e.setCancelled(true);
			}
		}
		else if(entity instanceof Player)
		{
			Player player = (Player) entity;
			Entity killer = damager; 
			
			double damage = e.getDamage();
	        double playerHealth = player.getHealth();
			
	        if (playerHealth - damage <= 0)
			{
	            e.setCancelled(true);
	            player.setHealth(100);
	    		player.setMaxHealth(100);
	    		player.setHealthScale(20);
	    		player.setHealthScaled(true);
	            player.setFoodLevel(10);
	            player.setGameMode(GameMode.SPECTATOR);
	            Bukkit.getConsoleSender().sendMessage(player.getName() + " was killed by " + killer.getName());
	            
	            gameManager.playersRemaining();
	        }
		}
		else if(entity.equals(EntityType.PLAYER))
		{
			e.setCancelled(true);
		}
	}
}
