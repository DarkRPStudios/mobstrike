package com.p376em.MobStrike.Events.Player;

import java.util.Collection;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PlayerMove implements Listener
{
	@EventHandler
	public void playerMove(PlayerMoveEvent e)
	{
		Player player = e.getPlayer();
		World world = player.getLocation().getWorld();
		Location playerLoc = player.getLocation();
		Collection<Entity> nearEntity1Block = world.getNearbyEntities(playerLoc, 1, 1, 1);
		Collection<Entity> nearEntity6Block = world.getNearbyEntities(playerLoc, 6, 6, 6);		
		
		for(Entity mob : nearEntity1Block)
		{
			EntityType mobType = mob.getType(); 
			
			if(mobType == EntityType.CAVE_SPIDER)
			{
				player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 120, 1));
			}
			else if(mobType == EntityType.MAGMA_CUBE)
			{
				player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 4));
			}
			else if(mobType == EntityType.SLIME)
			{
				player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 60, 2));
			}
		}
		
		for(Entity mob : nearEntity6Block)
		{
			EntityType mobType = mob.getType(); 
			
			if(mobType == EntityType.ENDERMAN)
			{
				player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 100, 1));
			}
		}
	}
}
