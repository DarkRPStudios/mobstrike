package com.p376em.MobStrike.Events.Player;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Utils.StringUtils;

public class PlayerDrop implements Listener
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration settings = null;
	
	@EventHandler
	public void PlayerDrop(PlayerDropItemEvent e)
	{
		Player player = e.getPlayer();
		
		settings = plugin.getSettingsConfig();
		
		if(settings.getBoolean("Settings.RegenerateWorld.BuildMode") == false)
		{
			e.setCancelled(true);
			player.sendMessage(StringUtils.color("&4You are not allowed to drop items"));
		}
	}	
}
