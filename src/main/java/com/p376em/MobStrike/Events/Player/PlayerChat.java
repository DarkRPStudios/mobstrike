package com.p376em.MobStrike.Events.Player;

import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.p376em.MobStrike.Events.Game.GameManager;
import com.p376em.MobStrike.Events.Game.GameState;
import com.p376em.MobStrike.Events.Game.GameStatus;
import com.p376em.MobStrike.Utils.StringUtils;

public class PlayerChat implements Listener
{
	private GameManager gameManager = new GameManager();

	@EventHandler
	public void playerChat(AsyncPlayerChatEvent e)
	{
		UUID senderUUID = e.getPlayer().getUniqueId();
		String msg = StringUtils.color(e.getMessage());
		String playerName = e.getPlayer().getName();

		if (GameStatus.getGameStatus().equals(GameState.INGAME))
		{
			e.setCancelled(true);

			String senderTeam = gameManager.getPlayerTeams().get(senderUUID);

			if (e.getMessage().startsWith("!"))
			{
				Bukkit.getConsoleSender().sendMessage(StringUtils.color("&4(Global) " + playerName + " &f- &r" + msg));
			}
			else
			{
				sendMSG(senderTeam, msg, playerName);
			}
		}
	}

	public void sendMSG(String team, String msg, String playerName)
	{
		if (team.equalsIgnoreCase("red"))
		{
			for (Entry<UUID, String> playerTeams : gameManager.getPlayerTeams().entrySet())
			{
				Player player = Bukkit.getServer().getPlayer(playerTeams.getKey());
				player.sendMessage(StringUtils.color("&c(" + team.toUpperCase() + ") " + playerName + " &f- &r" + msg));
			}
		} else if (team.equalsIgnoreCase("blue"))
		{
			for (Entry<UUID, String> playerTeams : gameManager.getPlayerTeams().entrySet())
			{
				Player player = Bukkit.getServer().getPlayer(playerTeams.getKey());
				player.sendMessage(StringUtils.color("&1(" + team.toUpperCase() + ") " + playerName + " &f- &r" + msg));
			}
		}
	}
}
