package com.p376em.MobStrike.Events.Player;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Events.Game.GameManager;
import com.p376em.MobStrike.Utils.StringUtils;

public class PlayerQuit implements Listener
{
	private GameManager gameManager = new GameManager();
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	private FileConfiguration gamedata = null;
	
	@EventHandler
	public void playerQuit(PlayerQuitEvent e)
	{
		Player player = e.getPlayer();
		
		int gameID = plugin.getSettingsConfig().getInt("Settings.Games.PlayGameID");
		int maxAllowedInGame = plugin.getGamesConfig().getInt("Games." + gameID + ".Settings.MaxPlayers");
		
		int redTeam = 0;
		int blueTeam = 0;
		
		removeFromAllMaps(player);
		
		for(Player p : Bukkit.getOnlinePlayers())
		{
			String teams = gameManager.getPlayerTeams().get(p.getUniqueId());
			System.out.println(teams);
			if(teams.equalsIgnoreCase("Red"))
			{
				redTeam = redTeam + 1;
				System.out.println(redTeam);
			}
			else if(teams.equalsIgnoreCase("Blue"))
			{
				blueTeam = blueTeam + 1;
				System.out.println(blueTeam);
			}
		}
		
		int remainingPlayers = Bukkit.getOnlinePlayers().size() - 1;
		
		System.out.println(redTeam);
		System.out.println(blueTeam);
		
		if(blueTeam == 0)
		{
			gameManager.roundEnd("red");
		}
		else if(redTeam == 0)
		{
			gameManager.roundEnd("blue");
		}
		
		for (final Player allPlayer : Bukkit.getOnlinePlayers())
		{
            allPlayer.sendMessage(StringUtils.color("&1" + player.getName() + " has just left the server &d(" + remainingPlayers + "/" + maxAllowedInGame + ")" ));
        }
	}

	private void removeFromAllMaps(Player player)
	{
		gamedata = plugin.getGameDataConfig();
		
		UUID uuid = player.getUniqueId();
		
		gameManager.removePlayers(uuid);
		gameManager.removePlayerSpawnPoints(uuid);
		gameManager.removePlayerTeam(uuid);
		
		gamedata.set("GameData." + uuid , null);
		
		plugin.saveGameDataConfig(gamedata);
		gamedata = null;
		
		System.out.println("player removed from all maps");
	}
}
