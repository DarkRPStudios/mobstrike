package com.p376em.MobStrike.Events.Player;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Managers.Inventory.InventoryManager;
import com.p376em.MobStrike.Utils.StringUtils;

public class PlayerInteractEntity implements Listener
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration shop = plugin.getShopConfig();
	FileConfiguration armoury = plugin.getArmouryConfig();
	FileConfiguration settings = plugin.getSettingsConfig();
	private InventoryManager invManager = new InventoryManager();
	
	int shopID = settings.getInt("Settings.Games.ShopID");
	int armouryID = settings.getInt("Settings.Games.ArmouryID");
		
	@EventHandler
	public void playerInteract(PlayerInteractEntityEvent e)
	{
		Player player =  e.getPlayer();
		Entity entity = e.getRightClicked();
		EntityType entityType = e.getRightClicked().getType();
		String shopNPCName, armouryNPCName;
		
		if(shop.getString("Shop." + shopID + ".Settings.shopNPCName") != null || armoury.getString("Armoury." + armouryID + ".Settings.armouryNPCName") != null)
		{
			shopNPCName = StringUtils.color(shop.getString("Shop." + shopID + ".Settings.shopNPCName"));
			armouryNPCName = StringUtils.color(armoury.getString("Armoury." + armouryID + ".Settings.armouryNPCName"));
			
			if(entityType == EntityType.VILLAGER)
			{
				if(entity.getCustomName().equalsIgnoreCase(shopNPCName))
				{
					e.setCancelled(true);
					
					invManager.shopInventory(player);
				}
				else if(entity.getCustomName().equalsIgnoreCase(armouryNPCName))
				{
					e.setCancelled(true);
					
					invManager.armouryInventory(player);
				}
				
				e.setCancelled(true);
			}
		}
	}
}
