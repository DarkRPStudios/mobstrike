package com.p376em.MobStrike.Events.Player;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Events.Game.GameManager;
import com.p376em.MobStrike.Events.Game.GameStatus;
import com.p376em.MobStrike.Utils.LocationBuilder;
import com.p376em.MobStrike.Utils.StringUtils;


public class PlayerJoin implements Listener
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration settings = plugin.getSettingsConfig();	
	FileConfiguration games = plugin.getGamesConfig();
	private GameManager gameManager = new GameManager();
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e)
	{
		Player player = e.getPlayer();
			
		player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(100.0D);
		
		if(settings.getInt("Settings.Games.PlayGameID") == 0)
		{
			
			player.sendMessage(StringUtils.color("&eThis server does not have a valid game"));
		}
		else if(settings.getInt("Settings.Games.ShopID") == 0)
		{

			player.sendMessage(StringUtils.color("&eThis server does not have a valid shop"));
		}
		
		if(settings.getBoolean("Settings.RegenerateWorld.BuildMode") == true)
		{
			player.setGameMode(GameMode.CREATIVE);
		}
		
		if(settings.getInt("Settings.Games.PlayGameID") > 0 && settings.getInt("Settings.Games.ShopID") > 0 && settings.getBoolean("Settings.RegenerateWorld.BuildMode") == false)
		{
			
			
			int gameID = settings.getInt("Settings.Games.PlayGameID");
			int playersOnline = Bukkit.getOnlinePlayers().size();
			int maxAllowedInGame = games.getInt("Games." + gameID + ".Settings.MaxPlayers");
				
			player.setMaxHealth(100);
			player.setHealthScale(20);
			player.setHealthScaled(true);
			player.setHealth(100);
			player.setFoodLevel(20);
			player.setGameMode(GameMode.SURVIVAL);
			player.getInventory().clear();
			
			ItemStack redConcrete = new ItemStack(Material.RED_CONCRETE, 1);
			ItemStack blueConcrete = new ItemStack(Material.BLUE_CONCRETE, 1);
			
			ItemMeta redItemMeta = redConcrete.getItemMeta();
			ItemMeta blueItemMeta = blueConcrete.getItemMeta();
			
			redItemMeta.setDisplayName(StringUtils.color("&4Vote To Join Red Team"));
			blueItemMeta.setDisplayName(StringUtils.color("&1Vote To Join Blue Team"));
			
			redConcrete.setItemMeta(redItemMeta);
			blueConcrete.setItemMeta(blueItemMeta);
			
			player.getInventory().setItem(2, redConcrete);
			player.getInventory().setItem(6, blueConcrete);
			player.updateInventory();
				
			gameManager.setPlayers(player.getUniqueId());
			
			for (Player allPlayer : Bukkit.getOnlinePlayers())
            {
                allPlayer.sendMessage(StringUtils.color("&1" + player.getName() + " has just joined the server &d(" + playersOnline + "/" + maxAllowedInGame + ")"));
            }
			
			String lobbyWorld = games.getString("Games." + gameID + ".Lobby.world");
			double x = games.getDouble("Games." + gameID + ".Lobby.x");
			double y = games.getDouble("Games." + gameID + ".Lobby.y");
			double z = games.getDouble("Games." + gameID + ".Lobby.z");
			
			Location tpLoc = LocationBuilder.basicLocation(lobbyWorld, x, y, z);
			player.teleport(tpLoc);
		}
	}
}
