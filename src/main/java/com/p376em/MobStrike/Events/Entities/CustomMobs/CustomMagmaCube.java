package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.MagmaCube;
import org.bukkit.inventory.EntityEquipment;

public class CustomMagmaCube
{
	public void magmaCubeMob(Location loc, World world, String name)
	{
		MagmaCube magmaCube = (MagmaCube) world.spawnEntity(loc, EntityType.MAGMA_CUBE);
		
		magmaCube.setCustomName(name);
		magmaCube.setCustomNameVisible(true);
		
		EntityEquipment equipment = magmaCube.getEquipment();
	}
}
