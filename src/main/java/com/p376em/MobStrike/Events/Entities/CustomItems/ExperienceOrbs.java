package com.p376em.MobStrike.Events.Entities.CustomItems;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ExperienceOrb;

import com.p376em.MobStrike.MobStrike;

public class ExperienceOrbs
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	
	FileConfiguration shop = plugin.getShopConfig();
	FileConfiguration games = plugin.getGamesConfig();
	FileConfiguration settings = plugin.getSettingsConfig();
	
	int gameID = settings.getInt("Settings.Games.PlayGameID");
	int maxEXP = 1;
	
	public void spawnEXPorbs()
	{
		int i = 0;
		String gameWorld;
		double gameX;
		double gameY;
		double gameZ;
		World spawnWorld;
		Location spawnLoc;
		
		for(i = 1; i <= maxEXP; i++)
		{
			gameWorld = games.getString("Games." + gameID + ".NPC.Red." + i + ".world");
			gameX = games.getDouble("Games." + gameID + ".NPC.Red." + i + ".x");
			gameY = games.getDouble("Games." + gameID + ".NPC.Red." + i + ".y") + 2;
			gameZ = games.getDouble("Games." + gameID + ".NPC.Red." + i + ".z");
			
			spawnWorld = Bukkit.getWorld(gameWorld); 
			spawnLoc = new Location(spawnWorld, gameX, gameY, gameZ);
					
			ExperienceOrb spawnEXPred = (ExperienceOrb) spawnWorld.spawnEntity(spawnLoc, EntityType.EXPERIENCE_ORB);
			spawnEXPred.setCustomName("exp");
			spawnEXPred.setCustomNameVisible(false);
			spawnEXPred.setExperience(10);
			
			gameWorld = games.getString("Games." + gameID + ".NPC.Blue." + i + ".world");
			gameX = games.getDouble("Games." + gameID + ".NPC.Blue." + i + ".x");
			gameY = games.getDouble("Games." + gameID + ".NPC.Blue." + i + ".y") + 2;
			gameZ = games.getDouble("Games." + gameID + ".NPC.Blue." + i + ".z");
			
			spawnWorld = Bukkit.getWorld(gameWorld); 
			spawnLoc = new Location(spawnWorld, gameX, gameY, gameZ);
					
			ExperienceOrb spawnEXPblue = (ExperienceOrb) spawnWorld.spawnEntity(spawnLoc, EntityType.EXPERIENCE_ORB);
			spawnEXPblue.setCustomName("exp");
			spawnEXPblue.setCustomNameVisible(false);
			spawnEXPblue.setExperience(10);
			
		}    
	}
}
