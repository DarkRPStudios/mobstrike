package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Slime;
import org.bukkit.inventory.EntityEquipment;

public class CustomSlime
{
	public void slimeMob(Location loc, World world, String name)
	{
		Slime slime = (Slime) world.spawnEntity(loc, EntityType.SLIME);
		
		slime.setCustomName(name);
		slime.setCustomNameVisible(true);
		
		EntityEquipment equipment = slime.getEquipment();
	}
}
