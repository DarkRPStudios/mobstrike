package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Stray;
import org.bukkit.inventory.EntityEquipment;

public class CustomStray
{
	public void strayMob(Location loc, World world, String name)
	{
		Stray stray = (Stray) world.spawnEntity(loc, EntityType.STRAY);
		
		stray.setCustomName(name);
		stray.setCustomNameVisible(true);
		
		EntityEquipment equipment = stray.getEquipment();
	}
}
