package com.p376em.MobStrike.Events.Entities.CustomMobs;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Managers.EXP.EXPManager;
import com.p376em.MobStrike.Utils.LocationBuilder;
import com.p376em.MobStrike.Utils.StringUtils;

public class MobManager
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	private EXPManager expManager = new EXPManager();
	FileConfiguration settings = plugin.getSettingsConfig();	
	FileConfiguration shop = plugin.getShopConfig();
	FileConfiguration games = plugin.getGamesConfig();
	FileConfiguration gamedata = null;
	private CustomBlaze blaze = new CustomBlaze();
	private CustomEnderman enderman = new CustomEnderman();
	private CustomEvoker evoker = new CustomEvoker();
	private CustomGhast ghast = new CustomGhast();
	private CustomGuardian guardian = new CustomGuardian();
	private CustomHusk husk = new CustomHusk();
	private CustomIllusioner illusioner = new CustomIllusioner();
	private CustomMagmaCube magma = new CustomMagmaCube();
	private CustomPigZombie pigzombie = new CustomPigZombie();
	private CustomRabbit rabbit = new CustomRabbit();
	private CustomShulker shulker = new CustomShulker();
	private CustomSilverfish silverfish = new CustomSilverfish();
	private CustomSkeleton skeleton = new CustomSkeleton();
	private CustomSlime slime = new CustomSlime();
	private CustomSpider spider = new CustomSpider();
	private CustomSpiderJockey spiderJockey = new CustomSpiderJockey();
	private CustomStray stray = new CustomStray();
	private CustomVindicator vindicator = new CustomVindicator();
	private CustomWitch witch = new CustomWitch();
	private CustomZombie zombie = new CustomZombie();
	private CustomDrowned drowned = new CustomDrowned();
	private CustomPhantom phantom = new CustomPhantom();
	
	public void spawnChecker(Player player, int itemID)
	{
		gamedata = plugin.getGameDataConfig();
		
		int shopID = settings.getInt("Settings.Games.ShopID");
		
		String mobTeam = null;
		
		String mobName = shop.getString("Shop." + shopID + ".Items." + itemID + ".Name"); 
		int mobSpawnEXP = shop.getInt("Shop." + shopID + ".Items." + itemID + ".EXPLevels"); 
		String playerTeam = gamedata.getString("GameData." + player.getUniqueId() + ".Team");
				
		if(expManager.payEXP(player.getUniqueId(), mobSpawnEXP))
		{
			if(playerTeam.equalsIgnoreCase("Red"))
			{
				mobTeam = "Blue";
			}
			else if(playerTeam.equalsIgnoreCase("Blue"))
			{
				mobTeam = "Red";
			}
			
			if(mobName.equalsIgnoreCase("guardian"))
			{
				spawnMob(spawnRandomiser("Water", mobName, mobTeam), player, itemID, mobName);
			}
			else if(mobName.equalsIgnoreCase("shulker") || mobName.equalsIgnoreCase("ghast"))
			{
				spawnMob(spawnRandomiser("Shulker", mobName, mobTeam), player, itemID, mobName);
			}
			else
			{
				spawnMob(spawnRandomiser("Land", mobName, mobTeam), player, itemID, mobName);
			}
		}
		else
		{
			int levels = (mobSpawnEXP - (expManager.getPlayerXP(player.getUniqueId())));
			player.sendMessage(StringUtils.color("&4You do not have enough XP to spawn a " + mobName + "\nYou need " + levels + " more EXP Levels"));
			return;
		}
		
		
	}
	
	public Location spawnRandomiser(String landTypeMob, String mobName, String team)
	{
		Random rand = new Random();
		Location loc = null;
		int randSpawn = 1;
		String landType = landTypeMob.substring(0, 1).toUpperCase() + landTypeMob.substring(1).toLowerCase();
		
		int gameID = settings.getInt("Settings.Games.PlayGameID");
				
		int randomSize = games.getConfigurationSection("Games." + gameID + ".Mobs." + team + "." + landType).getKeys(false).size();
		
		if(randomSize > 1)
		{
			randSpawn = 1 + rand.nextInt(randomSize);
		}
			
		String allowed = games.getString("Games." + gameID + ".Mobs." + team + "." + landType + "." + randSpawn + ".AllowedMobs");
		
		while(loc == null)
		{
		
			if(allowed.contains(":"))
			{
				String[] mobArray = games.getString("Games." + gameID + ".Mobs." + team + "." + landType + "." + randSpawn + ".AllowedMobs").split(":");
				
				for(int i = 0; i <= mobArray.length; i++)
				{
					if(mobArray[i].equalsIgnoreCase(mobName))
					{
						String gameWorld = games.getString("Games." + gameID + ".Mobs." + team + "." + landType + "." + i + ".world");
			    		double gameX = games.getDouble("Games." + gameID + ".Mobs." + team + "." + landType + "." + i + ".x");
			    		double gameY = games.getDouble("Games." + gameID + ".Mobs." + team + "." + landType + "." + i + ".y");
			    		double gameZ = games.getDouble("Games." + gameID + ".Mobs." + team + "." + landType + "." + i + ".z");
			    		
			    		loc = LocationBuilder.basicLocation(gameWorld, gameX, gameY, gameZ);
			    		
					}
				}
			}
			else
			{
				String mobArray = games.getString("Games." + gameID + ".Mobs." + team + "." + landType + "." + randSpawn + ".AllowedMobs");
				
				if(mobArray.equalsIgnoreCase(mobName))
				{
					if(mobName.equalsIgnoreCase("ghast"))
					{
						String gameWorld = games.getString("Games." + gameID + ".Mobs." + team + "." + landType + "." + randSpawn + ".world");
			    		double gameX = games.getDouble("Games." + gameID + ".Mobs." + team + "." + landType + "." + randSpawn + ".x");
			    		double gameY = games.getDouble("Games." + gameID + ".Mobs." + team + "." + landType + "." + randSpawn + ".y") + 5;
			    		double gameZ = games.getDouble("Games." + gameID + ".Mobs." + team + "." + landType + "." + randSpawn + ".z");
			    		
			    		loc = LocationBuilder.basicLocation(gameWorld, gameX, gameY, gameZ);
					}
					else
					{
						String gameWorld = games.getString("Games." + gameID + ".Mobs." + team + "." + landType + "." + randSpawn + ".world");
			    		double gameX = games.getDouble("Games." + gameID + ".Mobs." + team + "." + landType + "." + randSpawn + ".x");
			    		double gameY = games.getDouble("Games." + gameID + ".Mobs." + team + "." + landType + "." + randSpawn + ".y");
			    		double gameZ = games.getDouble("Games." + gameID + ".Mobs." + team + "." + landType + "." + randSpawn + ".z");
			    		
			    		loc = LocationBuilder.basicLocation(gameWorld, gameX, gameY, gameZ);
					}
				}
				else if(mobArray.equalsIgnoreCase("all"))
				{
					String gameWorld = games.getString("Games." + gameID + ".Mobs." + team + "." + landType + "." + randSpawn + ".world");
		    		double gameX = games.getDouble("Games." + gameID + ".Mobs." + team + "." + landType + "." + randSpawn + ".x");
		    		double gameY = games.getDouble("Games." + gameID + ".Mobs." + team + "." + landType + "." + randSpawn + ".y");
		    		double gameZ = games.getDouble("Games." + gameID + ".Mobs." + team + "." + landType + "." + randSpawn + ".z");
		    		
		    		loc = LocationBuilder.basicLocation(gameWorld, gameX, gameY, gameZ);
				}
			}
		}
		
		return loc;
	}
	
	public void spawnMob(Location spawnLoc, Player player, int itemID, String mobName)
	{
		String playerName = player.getName();
		String name = playerName + "'s " + mobName;
		if(spawnLoc.equals(null))
		{
			player.sendMessage(StringUtils.color("&4An error has occured spawning that mob"));
			return;
		}

		switch(mobName.toLowerCase())
		{
		case "blaze":
			blaze.blazeMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "enderman":
			enderman.endermanMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "endermite":
			enderman.endermiteMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "evoker":
			evoker.evokerMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "ghast":
			ghast.ghastMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "guardian":
			guardian.guardianMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "husk":
			husk.huskMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "illusioner":
			illusioner.illusionerMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "magma cube":
			magma.magmaCubeMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "pigzombie":
			pigzombie.PigZombieMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "baby pigzombie":
			pigzombie.babyPigZombieMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "rabbit":
			rabbit.rabbitMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "killer bunny":
			rabbit.killerBunnyMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "shulker":
			shulker.shulkerMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "silverfish":
			silverfish.silverfishMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "skeleton":
			skeleton.skeletonMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "wither skeleton":
			skeleton.witherSkeletonMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "nether skeleton":
			skeleton.netherSkeletonMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "slime":
			slime.slimeMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "spider":
			spider.spiderMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "cave spider":
			spider.caveSpiderMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "spider jockey":
			spiderJockey.spiderWitherSkeletonMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "stray":
			stray.strayMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "vindicator":
			vindicator.vindicatorMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "witch":
			witch.witchMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "zombie":
			zombie.zombieMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "drowned":
			drowned.drownedMobWithTrident(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		case "phantom":
			phantom.phantomMob(spawnLoc, spawnLoc.getWorld(), name);
			break;
			
		}
	}
}
