package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;

import com.p376em.MobStrike.Utils.StringUtils;

public class ShopVillager
{
	public void spawnVillager(Location spawnLoc, World spawnWorld, FileConfiguration shop, int shopID)
	{
		for(Entity entity:spawnWorld.getNearbyEntities(spawnLoc, 1, 1, 1))
		{
			entity.remove();
		}
				
		Villager spawnVillager = (Villager) spawnWorld.spawnEntity(spawnLoc, EntityType.VILLAGER);
        spawnVillager.setCustomName(StringUtils.color(shop.getString("Shop." + shopID + ".Settings.shopNPCName")));
        spawnVillager.setInvulnerable(true);
        spawnVillager.setProfession(Villager.Profession.NITWIT);
        spawnVillager.setAI(false);
        spawnVillager.setCollidable(false);
	}
}
