package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Shulker;
import org.bukkit.inventory.EntityEquipment;

public class CustomShulker
{
	public void shulkerMob(Location loc, World world, String name)
	{
		Shulker shulker = (Shulker) world.spawnEntity(loc, EntityType.SHULKER);
		
		shulker.setCustomName(name);
		shulker.setCustomNameVisible(true);
		
		EntityEquipment equipment = shulker.getEquipment();
	}
}
