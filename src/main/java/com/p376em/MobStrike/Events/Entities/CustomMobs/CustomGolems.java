package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.Snowman;
import org.bukkit.inventory.EntityEquipment;

public class CustomGolems
{
	public void snowGolemMob(Location loc, World world, String name)
	{
		Snowman snowGolem = (Snowman) world.spawnEntity(loc, EntityType.SNOWMAN);
		
		snowGolem.setCustomName(name);
		snowGolem.setCustomNameVisible(true);
		
		EntityEquipment equipment = snowGolem.getEquipment();
	}
	
	public void ironGolemMob(Location loc, World world, String name)
	{
		IronGolem ironGolem = (IronGolem) world.spawnEntity(loc, EntityType.IRON_GOLEM);
				
		ironGolem.setCustomName(name);
		ironGolem.setCustomNameVisible(true);
		
		EntityEquipment equipment = ironGolem.getEquipment();
	}
}
