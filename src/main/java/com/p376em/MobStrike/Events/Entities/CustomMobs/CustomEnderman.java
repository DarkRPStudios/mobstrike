package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Endermite;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.EntityEquipment;

public class CustomEnderman
{
	public void endermanMob(Location loc, World world, String name)
	{
		Enderman enderman = (Enderman) world.spawnEntity(loc, EntityType.ENDERMAN);
		
		enderman.setCustomName(name);
		enderman.setCustomNameVisible(true);
				
		EntityEquipment equipment = enderman.getEquipment();
	}
	
	public void endermiteMob(Location loc, World world, String name)
	{
		Endermite endermite = (Endermite) world.spawnEntity(loc, EntityType.ENDERMITE);
		
		endermite.setCustomName(name);
		endermite.setCustomNameVisible(true);
		
		EntityEquipment equipment = endermite.getEquipment();
	}
}
