package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.CaveSpider;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Spider;
import org.bukkit.inventory.EntityEquipment;

public class CustomSpider
{
	public void spiderMob(Location loc, World world, String name)
	{
		Spider spider = (Spider) world.spawnEntity(loc, EntityType.SPIDER);
				
		spider.setCustomName(name);
		spider.setCustomNameVisible(true);
		
		EntityEquipment equipment = spider.getEquipment();
	}
	
	public void caveSpiderMob(Location loc, World world, String name)
	{
		CaveSpider spider = (CaveSpider) world.spawnEntity(loc, EntityType.CAVE_SPIDER);
				
		spider.setCustomName(name);
		spider.setCustomNameVisible(true);
		
		EntityEquipment equipment = spider.getEquipment();
	}
}
