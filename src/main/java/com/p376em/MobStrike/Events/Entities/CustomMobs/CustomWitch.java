package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Witch;
import org.bukkit.inventory.EntityEquipment;

public class CustomWitch
{
	public void witchMob(Location loc, World world, String name)
	{
		Witch witch = (Witch) world.spawnEntity(loc, EntityType.WITCH);
		
		witch.setCustomName(name);
		witch.setCustomNameVisible(true);
		
		EntityEquipment equipment = witch.getEquipment();
	}
}
