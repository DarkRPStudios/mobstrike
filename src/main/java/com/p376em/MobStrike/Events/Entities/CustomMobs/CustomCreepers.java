package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.EntityEquipment;

public class CustomCreepers
{
	public void creeperMob(Location loc, World world, String name)
	{
		Creeper creeper = (Creeper) world.spawnEntity(loc, EntityType.CREEPER);
		
		creeper.setCustomName(name);
		creeper.setCustomNameVisible(true);
		
		EntityEquipment equipment = creeper.getEquipment();
	}
	
	public void chargedCreeperMob(Location loc, World world, String name)
	{
		Creeper creeper = (Creeper) world.spawnEntity(loc, EntityType.CREEPER);
		
		creeper.setPowered(true);
		
		creeper.setCustomName(name);
		creeper.setCustomNameVisible(true);
		
		EntityEquipment equipment = creeper.getEquipment();
	}
}
