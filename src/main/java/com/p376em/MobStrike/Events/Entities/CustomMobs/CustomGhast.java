package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Ghast;
import org.bukkit.inventory.EntityEquipment;

public class CustomGhast
{
	public void ghastMob(Location loc, World world, String name)
	{
		Ghast ghast = (Ghast) world.spawnEntity(loc, EntityType.GHAST);
		
		ghast.setCustomName(name);
		ghast.setCustomNameVisible(true);
		
		EntityEquipment equipment = ghast.getEquipment();
	}
}
