package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Evoker;
import org.bukkit.inventory.EntityEquipment;

public class CustomEvoker
{
	public void evokerMob(Location loc, World world, String name)
	{
		Evoker evoker = (Evoker) world.spawnEntity(loc, EntityType.EVOKER);
		
		evoker.setCustomName(name);
		evoker.setCustomNameVisible(true);
		
		EntityEquipment equipment = evoker.getEquipment();
	}
}
