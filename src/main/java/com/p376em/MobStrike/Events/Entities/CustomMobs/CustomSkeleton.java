package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.WitherSkeleton;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;

public class CustomSkeleton
{
	public void skeletonMob(Location loc, World world, String name)
	{
		Skeleton skeleton = (Skeleton) world.spawnEntity(loc, EntityType.SKELETON);
		
		skeleton.setCustomName(name);
		skeleton.setCustomNameVisible(true);
		
		EntityEquipment equipment = skeleton.getEquipment();
	}
	
	public void witherSkeletonMob(Location loc, World world, String name)
	{
		WitherSkeleton skeleton = (WitherSkeleton) world.spawnEntity(loc, EntityType.WITHER_SKELETON);
		
		skeleton.setCustomName(name);
		skeleton.setCustomNameVisible(true);
		
		EntityEquipment equipment = skeleton.getEquipment();
	}
	
	public void netherSkeletonMob(Location loc, World world, String name)
	{
		WitherSkeleton skeleton = (WitherSkeleton) world.spawnEntity(loc, EntityType.WITHER_SKELETON);
		
		skeleton.setCustomName(name);
		skeleton.setCustomNameVisible(true);
		
		EntityEquipment equipment = skeleton.getEquipment();
		equipment.setItemInMainHand(new ItemStack(Material.BOW));
	}
}
