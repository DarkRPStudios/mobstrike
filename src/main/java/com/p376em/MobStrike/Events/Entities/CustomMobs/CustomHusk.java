package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Husk;
import org.bukkit.inventory.EntityEquipment;

public class CustomHusk
{
	public void huskMob(Location loc, World world, String name)
	{
		Husk husk = (Husk) world.spawnEntity(loc, EntityType.HUSK);
		
		husk.setCustomName(name);
		husk.setCustomNameVisible(true);
		
		EntityEquipment equipment = husk.getEquipment();
	}
}
