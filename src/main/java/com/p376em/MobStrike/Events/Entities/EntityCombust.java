package com.p376em.MobStrike.Events.Entities;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustEvent;

public class EntityCombust implements Listener
{
	@EventHandler
	public void entityCombust(EntityCombustEvent e)
	{
		Entity entity = e.getEntity();
		
		if(
				entity.getType().equals(EntityType.PHANTOM) || 
				entity.getType().equals(EntityType.DROWNED) || 
				entity.getType().equals(EntityType.SKELETON) || 
				entity.getType().equals(EntityType.STRAY) || 
				entity.getType().equals(EntityType.ZOMBIE))
		{
			e.setCancelled(false);
		}
	}
}
