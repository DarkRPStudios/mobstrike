package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Spider;
import org.bukkit.entity.WitherSkeleton;
import org.bukkit.entity.Zombie;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;

public class CustomSpiderJockey
{
	public void spiderWitherSkeletonMob(Location loc, World world, String name)
	{
		Spider spider = (Spider) world.spawnEntity(loc, EntityType.SPIDER);
		WitherSkeleton skelly = (WitherSkeleton) world.spawnEntity(loc, EntityType.WITHER_SKELETON);
		
		spider.addPassenger(skelly);
		
		skelly.setCustomName(name);
		skelly.setCustomNameVisible(true);
		
		EntityEquipment equipment = skelly.getEquipment();
		equipment.setItemInMainHand(new ItemStack(Material.STONE_SWORD));
	}
	
	public void spiderZombieMob(Location loc, World world, String name)
	{
		Spider spider = (Spider) world.spawnEntity(loc, EntityType.SPIDER);
		Zombie zombie = (Zombie) world.spawnEntity(loc, EntityType.ZOMBIE);
		
		spider.addPassenger(zombie);
		
		zombie.setCustomName(name);
		zombie.setCustomNameVisible(true);
		
		EntityEquipment equipment = zombie.getEquipment();
		equipment.setItemInMainHand(new ItemStack(Material.STONE_SWORD));
	}
}
