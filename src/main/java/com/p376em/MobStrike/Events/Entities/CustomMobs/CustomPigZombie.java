package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.PigZombie;
import org.bukkit.inventory.EntityEquipment;

public class CustomPigZombie
{
	public void babyPigZombieMob(Location loc, World world, String name)
	{
		PigZombie pigZombie = (PigZombie) world.spawnEntity(loc, EntityType.PIG_ZOMBIE);
		
		pigZombie.setCustomName(name);
		pigZombie.setCustomNameVisible(true);
		
		pigZombie.setBaby(true);
		
		pigZombie.setAnger(99);
		pigZombie.setAngry(true);
		
		EntityEquipment equipment = pigZombie.getEquipment();		
	}
	
	public void PigZombieMob(Location loc, World world, String name)
	{
		PigZombie pigZombie = (PigZombie) world.spawnEntity(loc, EntityType.PIG_ZOMBIE);
		
		pigZombie.setCustomName(name);
		pigZombie.setCustomNameVisible(true);
		
		pigZombie.setAnger(99);
		pigZombie.setAngry(true);
				
		EntityEquipment equipment = pigZombie.getEquipment();		
	}
}
