package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Phantom;
import org.bukkit.inventory.EntityEquipment;

public class CustomPhantom
{
	public void phantomMob(Location loc, World world, String name)
	{
		Phantom phantom = (Phantom) world.spawnEntity(loc, EntityType.PHANTOM);
		
		phantom.setCustomName(name);
		phantom.setCustomNameVisible(true);
		
		EntityEquipment equipment = phantom.getEquipment();
	}
}
