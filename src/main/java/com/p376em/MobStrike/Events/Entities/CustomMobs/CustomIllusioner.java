package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Illusioner;
import org.bukkit.inventory.EntityEquipment;

public class CustomIllusioner
{
	public void illusionerMob(Location loc, World world, String name)
	{
		Illusioner illusioner = (Illusioner) world.spawnEntity(loc, EntityType.ILLUSIONER);
		
		illusioner.setCustomName(name);
		illusioner.setCustomNameVisible(true);
		
		EntityEquipment equipment = illusioner.getEquipment();
	}
}
