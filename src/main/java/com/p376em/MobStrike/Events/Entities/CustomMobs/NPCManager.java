package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

import com.p376em.MobStrike.MobStrike;

public class NPCManager
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	
	private ShopVillager shopVillager = new ShopVillager();
	private ArmouryVillager armouryVillager = new ArmouryVillager();
	
	FileConfiguration shop = plugin.getShopConfig();
	FileConfiguration games = plugin.getGamesConfig();
	FileConfiguration settings = plugin.getSettingsConfig();
	FileConfiguration armoury = plugin.getArmouryConfig();
	
	int gameID = settings.getInt("Settings.Games.PlayGameID");
	int shopID = settings.getInt("Settings.Games.ShopID");
	int armouryID = settings.getInt("Settings.Games.ArmouryID");
	int maxShopNPCs = games.getInt("Games." + gameID + ".Settings.MaxPlayers");
	int maxArmouryNPCs = games.getInt("Games." + gameID + ".Settings.MaxPlayers");
	
	
	public void shopKeepers()
	{
		int i = 0;
		
		for(i = 1; i <= maxShopNPCs; i++)
    	{
			if(games.getString("Games." + gameID + ".NPC.Shop." + i + ".world") != null)
			{
	    		String gameWorld = games.getString("Games." + gameID + ".NPC.Shop." + i + ".world");
	    		double gameX = games.getDouble("Games." + gameID + ".NPC.Shop." + i + ".x");
	    		double gameY = games.getDouble("Games." + gameID + ".NPC.Shop." + i + ".y");
	    		double gameZ = games.getDouble("Games." + gameID + ".NPC.Shop." + i + ".z");
	    		float gameYaw = (float) games.getDouble("Games." + gameID + ".NPC.Shop." + i + ".yaw");
	    		float gamePitch = (float) games.getDouble("Games." + gameID + ".NPC.Shop." + i + ".pitch");
	    		
	    		World spawnWorld = Bukkit.getWorld(gameWorld); 
	    		Location spawnLoc = new Location(spawnWorld, gameX, gameY, gameZ, gameYaw, gamePitch);
	    		
	    		shopVillager.spawnVillager(spawnLoc, spawnWorld, shop, shopID);
			}
    	}
	}
	
	public void blackSmiths()
	{
		int i = 0;
		
		for(i = 1; i <= maxArmouryNPCs; i++)
    	{
			if(games.getString("Games." + gameID + ".NPC.Armoury." + i + ".world") != null)
			{
	    		String gameWorld = games.getString("Games." + gameID + ".NPC.Armoury." + i + ".world");
	    		double gameX = games.getDouble("Games." + gameID + ".NPC.Armoury." + i + ".x");
	    		double gameY = games.getDouble("Games." + gameID + ".NPC.Armoury." + i + ".y");
	    		double gameZ = games.getDouble("Games." + gameID + ".NPC.Armoury." + i + ".z");
	    		float gameYaw = (float) games.getDouble("Games." + gameID + ".NPC.Armoury." + i + ".yaw");
	    		float gamePitch = (float) games.getDouble("Games." + gameID + ".NPC.Armoury." + i + ".pitch");
	    		
	    		World spawnWorld = Bukkit.getWorld(gameWorld); 
	    		Location spawnLoc = new Location(spawnWorld, gameX, gameY, gameZ, gameYaw, gamePitch);
	    		
	    		armouryVillager.spawnVillager(spawnLoc, spawnWorld, armoury, armouryID);
			}
    	}
	}
}
