package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Zombie;
import org.bukkit.entity.ZombieVillager;
import org.bukkit.inventory.EntityEquipment;

public class CustomZombie
{
	public void zombieMob(Location loc, World world, String name)
	{
		Zombie zombie = (Zombie) world.spawnEntity(loc, EntityType.ZOMBIE);
		
		zombie.setCustomName(name);
		zombie.setCustomNameVisible(true);
		
		EntityEquipment equipment = zombie.getEquipment();
	}
	
	public void zombieVillagerMob(Location loc, World world, String name)
	{
		ZombieVillager zombie = (ZombieVillager) world.spawnEntity(loc, EntityType.ZOMBIE_VILLAGER);
		
		zombie.setCustomName(name);
		zombie.setCustomNameVisible(true);
		
		EntityEquipment equipment = zombie.getEquipment();
	}
}
