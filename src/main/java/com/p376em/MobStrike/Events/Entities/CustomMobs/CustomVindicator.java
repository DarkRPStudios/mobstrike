package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Vindicator;
import org.bukkit.inventory.EntityEquipment;

public class CustomVindicator
{
	public void vindicatorMob(Location loc, World world, String name)
	{
		Vindicator vindicator = (Vindicator) world.spawnEntity(loc, EntityType.VINDICATOR);
		
		vindicator.setCustomName(name);
		vindicator.setCustomNameVisible(true);
		
		EntityEquipment equipment = vindicator.getEquipment();
	}
}
