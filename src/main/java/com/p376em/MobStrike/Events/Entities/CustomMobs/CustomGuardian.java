package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Guardian;
import org.bukkit.inventory.EntityEquipment;

public class CustomGuardian
{
	public void guardianMob(Location loc, World world, String name)
	{
		Guardian guardian = (Guardian) world.spawnEntity(loc, EntityType.GUARDIAN);
		
		guardian.setCustomName(name);
		guardian.setCustomNameVisible(true);
		
		EntityEquipment equipment = guardian.getEquipment();
	}
}
