package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;

import com.p376em.MobStrike.Utils.StringUtils;

public class ArmouryVillager
{
	public void spawnVillager(Location spawnLoc, World spawnWorld, FileConfiguration armoury, int armouryID)
	{
		for(Entity entity:spawnWorld.getNearbyEntities(spawnLoc, 1, 1, 1))
		{
			entity.remove();
		}
		
		Villager spawnVillager = (Villager) spawnWorld.spawnEntity(spawnLoc, EntityType.VILLAGER);
        spawnVillager.setCustomName(StringUtils.color(armoury.getString("Armoury." + armouryID + ".Settings.armouryNPCName")));
        spawnVillager.setInvulnerable(true);
        spawnVillager.setProfession(Villager.Profession.BLACKSMITH);
        spawnVillager.setCareer(Villager.Career.WEAPON_SMITH);
        spawnVillager.setAI(false);
        spawnVillager.setCollidable(false);
	}
}
