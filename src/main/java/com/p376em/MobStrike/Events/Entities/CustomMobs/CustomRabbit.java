package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Rabbit;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class CustomRabbit
{
	public void rabbitMob(Location loc, World world, String name)
	{
		Rabbit rabbit = (Rabbit) world.spawnEntity(loc, EntityType.RABBIT);
		
		rabbit.setCustomName(name);
		rabbit.setCustomNameVisible(true);
		
		rabbit.setRabbitType(Rabbit.Type.BROWN);
		
		EntityEquipment equipment = rabbit.getEquipment();
	}
	
	public void killerBunnyMob(Location loc, World world, String name)
	{
		Rabbit rabbit = (Rabbit) world.spawnEntity(loc, EntityType.RABBIT);
		
		rabbit.setCustomName(name);
		rabbit.setCustomNameVisible(true);
		
		rabbit.setRabbitType(Rabbit.Type.THE_KILLER_BUNNY);
		rabbit.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 999999999, 1), true);
		
		EntityEquipment equipment = rabbit.getEquipment();
	}
}
