package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Drowned;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;

public class CustomDrowned
{
	public void drownedMob(Location loc, World world, String name)
	{
		Drowned drowned = (Drowned) world.spawnEntity(loc, EntityType.DROWNED);
		
		drowned.setCustomName(name);
		drowned.setCustomNameVisible(true);
		drowned.setBaby(false);
		
		EntityEquipment equipment = drowned.getEquipment();
	}
	
	public void drownedMobWithTrident(Location loc, World world, String name)
	{
		Drowned drowned = (Drowned) world.spawnEntity(loc, EntityType.DROWNED);
		
		drowned.setCustomName(name);
		drowned.setCustomNameVisible(true);
		drowned.setBaby(false);
		
		EntityEquipment equipment = drowned.getEquipment();
		equipment.setItemInMainHand(new ItemStack(Material.TRIDENT));
	}
}
