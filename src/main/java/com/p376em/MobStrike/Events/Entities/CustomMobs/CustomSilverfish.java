package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Silverfish;
import org.bukkit.inventory.EntityEquipment;

public class CustomSilverfish
{
	public void silverfishMob(Location loc, World world, String name)
	{
		Silverfish silverfish = (Silverfish) world.spawnEntity(loc, EntityType.SILVERFISH);
		
		silverfish.setCustomName(name);
		silverfish.setCustomNameVisible(true);
		
		EntityEquipment equipment = silverfish.getEquipment();
	}
}
