package com.p376em.MobStrike.Events.Entities.CustomMobs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Blaze;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.EntityEquipment;

public class CustomBlaze
{
	public void blazeMob(Location loc, World world, String name)
	{
		Blaze blaze = (Blaze) world.spawnEntity(loc, EntityType.BLAZE);
		
		blaze.setCustomName(name);
		blaze.setCustomNameVisible(true);
		
		EntityEquipment equipment = blaze.getEquipment();
	}
}
