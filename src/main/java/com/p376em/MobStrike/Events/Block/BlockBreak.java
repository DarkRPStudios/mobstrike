package com.p376em.MobStrike.Events.Block;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Utils.StringUtils;

public class BlockBreak implements Listener
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration settings = null;
	
	private ArrayList<UUID> playerBreakMessageList = new ArrayList<UUID>();
	
	@EventHandler
	public void blockBreak(BlockBreakEvent e)
	{
		settings = plugin.getSettingsConfig();
		
		if(settings.getInt("Settings.Games.PlayGameID") != 0)
		{
			if(settings.getBoolean("Settings.RegenerateWorld.BuildMode") == false)
			{
				e.setCancelled(true);
				
				if(!playerBreakMessageList.contains(e.getPlayer().getUniqueId()))
				{
					e.getPlayer().sendMessage(StringUtils.color("&cYou are not allowed to break any map blocks"));
					playerBreakMessageList.add(e.getPlayer().getUniqueId());
				}
			}
			else if(settings.getBoolean("Settings.RegenerateWorld.BuildMode") == true)
			{
				return;
			}
		}
		
	}
	
}
