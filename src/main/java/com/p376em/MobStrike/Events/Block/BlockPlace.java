package com.p376em.MobStrike.Events.Block;

import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Events.Entities.CustomMobs.MobManager;
import com.p376em.MobStrike.Events.Game.GameState;
import com.p376em.MobStrike.Events.Game.GameStatus;
import com.p376em.MobStrike.Events.Game.MobsCooldown;
import com.p376em.MobStrike.Events.Game.VoteRegister;
import com.p376em.MobStrike.Utils.StringUtils;

public class BlockPlace implements Listener
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration settings = null;
	FileConfiguration shop = plugin.getShopConfig();
	private MobManager mobManager = new MobManager();
	private MobsCooldown mobCooldown = new MobsCooldown();
	private VoteRegister voteCollector = new VoteRegister();
	
	@EventHandler
	public void blockPlace(BlockPlaceEvent e)
	{
		Player player = e.getPlayer();
		UUID uuid = player.getUniqueId();
		Block block = e.getBlockPlaced();
		Material blockType = block.getType();
		
		settings = plugin.getSettingsConfig();
		
		int shopID = settings.getInt("Settings.Games.ShopID");
		int shopSize = shop.getConfigurationSection("Shop." + shopID + ".Items").getKeys(false).size();
		int i = 0;		
				
		if(settings.getBoolean("Settings.RegenerateWorld.BuildMode") == true)
		{
			return;
		}		
		
		if(GameStatus.getGameStatus().equals(GameState.WAITING) || GameStatus.getGameStatus().equals(GameState.STARTING))
		{
			if(blockType == Material.BLUE_CONCRETE)
			{
				voteCollector.voteRegister(player, "blue");
				e.setCancelled(true);
				return;
			}
			else if(blockType == Material.RED_CONCRETE)
			{
				voteCollector.voteRegister(player, "red");
				e.setCancelled(true);
				return;
			}
		}
		else if(GameStatus.getGameStatus().equals(GameState.INGAME))
		{
			for(i = 1; i <= shopSize; i++)
			{
				String triggerBlock = shop.getString("Shop." + shopID + ".Items." + i + ".TriggerBlock");
				Material trigger = Material.getMaterial(triggerBlock);

				if(blockType == trigger)
				{
					String uuidAndBlock = uuid + blockType.toString();
					Long cooldownTime = shop.getLong("Shop." + shopID + ".Items." + i + ".Cooldown");
					
					if(mobCooldown.getCooldownMap().containsKey(uuidAndBlock))
					{
						long secondsRemaining = ((mobCooldown.getCooldown(uuidAndBlock) / 1000) + cooldownTime) - (System.currentTimeMillis() / 1000);
						
						if(secondsRemaining > 0)
						{
							player.sendMessage(StringUtils.color("&4You cant spawn this mob for another " + secondsRemaining + " seconds"));
							e.setCancelled(true);
						}
						else
						{
							mobCooldown.updateCooldownMap(uuidAndBlock, System.currentTimeMillis());
							mobManager.spawnChecker(player, i);
							e.setCancelled(true);
							return;
						}
					}
					else
					{
						mobCooldown.setCooldownMap(uuidAndBlock, System.currentTimeMillis());
						mobManager.spawnChecker(player, i);
						e.setCancelled(true);
						return;
					}
				}
			}
		}
	}
}