package com.p376em.MobStrike.Events.World;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;

import com.p376em.MobStrike.MobStrike;

public class WorldRegenerator
{
	private MobStrike plugin = (MobStrike)MobStrike.getPlugin(MobStrike.class);
	FileConfiguration settings = plugin.getSettingsConfig();
	
	private boolean buildModeEnabled = settings.getBoolean("Settings.RegenerateWorld.BuildMode");
	private boolean regenEnabled = settings.getBoolean("Settings.RegenerateWorld.Enabled");
	private String worldName = settings.getString("Settings.RegenerateWorld.WorldName");
	private World gameWorld = Bukkit.getWorld(worldName);
	
	public void worldRegen()
	{
		if(buildModeEnabled == true)
		{
			gameWorld.setAutoSave(true);
			return;
		}
		else if(buildModeEnabled == false)
		{
			if(regenEnabled == true)
			{
				gameWorld.setAutoSave(false);
			}
			else if(regenEnabled == false)
			{
				gameWorld.setAutoSave(true);
				return;				
			}
		}
	}			
}
