package com.p376em.MobStrike.Events.World;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

public class WeatherChange implements Listener
{	
	@EventHandler
	public void weatherChange(WeatherChangeEvent e)
	{
		e.getWorld().setThundering(false);
		e.setCancelled(true);
		e.getWorld().setWeatherDuration(0);
		e.getWorld().setStorm(false);
	}
}
