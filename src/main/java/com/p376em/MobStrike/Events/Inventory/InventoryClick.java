package com.p376em.MobStrike.Events.Inventory;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.p376em.MobStrike.MobStrike;
import com.p376em.MobStrike.Utils.StringUtils;

public class InventoryClick implements Listener
{
    private MobStrike plugin = (MobStrike) MobStrike.getPlugin(MobStrike.class);
    FileConfiguration settings = plugin.getSettingsConfig();
    FileConfiguration shop = plugin.getShopConfig();
    FileConfiguration armoury = plugin.getArmouryConfig();
    FileConfiguration gamedata = null;
    int shopID = settings.getInt("Settings.Games.ShopID");
    int shopSize = shop.getConfigurationSection("Shop." + shopID + ".Items").getKeys(false).size();
    int armouryID = settings.getInt("Settings.Games.ArmouryID");
    int armourySize = armoury.getConfigurationSection("Armoury." + armouryID + ".Items").getKeys(false).size();

    @EventHandler
    public void inventoryClick(InventoryClickEvent e)
    {
        Player player = (Player) e.getWhoClicked();
        Inventory openInv = e.getClickedInventory();
        
        if (settings.getBoolean("Settings.RegenerateWorld.BuildMode"))
        {
            return;
        }
        
        int itemSlot = e.getSlot();
        Material item = e.getCurrentItem().getType();
        
        if (openInv.getTitle().equalsIgnoreCase(StringUtils.color(shop.getString("Shop." + shopID + ".Settings.shopName"))))
        {
	        e.setCancelled(true);
	        
	        for (int i = 1; i <= shopSize; i++)
	        {
	            String mobName = shop.getString("Shop." + shopID + ".Items." + i + ".Name");
	            int exp = shop.getInt("Shop." + shopID + ".Items." + i + ".EXPLevels");
	            Material mobItem = Material.getMaterial(shop.getString("Shop." + shopID + ".Items." + i + ".Item").toUpperCase());
	            Material mat = e.getCurrentItem().getType();
	            
	            if ((mat.equals(mobItem)))
                {
                    int itemID = i;
                    setPlayerShopInventory(player, item, itemID, mobName, exp);
                    return;
                }
	        }
        }
        
        if (openInv.getTitle().equalsIgnoreCase(StringUtils.color(armoury.getString("Armoury." + armouryID + ".Settings.armouryName"))))
        {
	        e.setCancelled(true);
	        
	        for (int i = 1; i <= armourySize; i++)
	        {
	            int armourySlot = armoury.getInt("Armoury." + armouryID + ".Items." + i + ".ItemSlot");
	            
	            if (itemSlot == armourySlot)
	            {
	                int itemID = i;
	                setPlayerArmouryInventory(player, itemID, armourySlot);
	                return;
	            }
	        }
        }
    }

    public void setPlayerShopInventory(Player player, Material item, int itemID, String mobName, int EXP)
    {
        gamedata = plugin.getGameDataConfig();
        
        if (gamedata.getConfigurationSection("GameData." + player.getUniqueId() + ".Inventory") == null)
        {
            gamedata.set("GameData." + player.getUniqueId() + ".Inventory", Integer.valueOf(0));
        }
        
        int inventorySize = gamedata.getConfigurationSection("GameData." + player.getUniqueId() + ".Inventory").getKeys(false).size();
        
        for (int i = 0; i <= inventorySize; i++)
        {
        	if (gamedata.getString("GameData." + player.getUniqueId() + ".Inventory." + i + ".item") == null)
            {
                gamedata.set("GameData." + player.getUniqueId() + ".Inventory." + i + ".item", item.name());
                gamedata.set("GameData." + player.getUniqueId() + ".Inventory." + i + ".itemID", Integer.valueOf(itemID));

                String trigger = shop.getString("Shop." + shopID + ".Items." + itemID + ".TriggerBlock");
                Material material = Material.getMaterial(trigger);

                ItemStack playerItem = new ItemStack(material);

                ItemMeta playerItemMeta = playerItem.getItemMeta();

                playerItemMeta.setDisplayName(StringUtils.color("&aSpawn a " + mobName + " - Requires " + EXP + "EXP Levels"));

                playerItem.setItemMeta(playerItemMeta);
                for (int in = 0; in < player.getInventory().getSize(); in ++)
                {
                    if (player.getInventory().getItem( in ) == null)
                    {
                        player.getInventory().setItem( in , playerItem);
                        return;
                    }
                }
                player.updateInventory();

                plugin.saveGameDataConfig(gamedata);
                gamedata = null;

                return;
            }
        }
    }

    public void setPlayerArmouryInventory(Player player, int itemID, int slot)
    {
        String block = armoury.getString("Armoury." + armouryID + ".Items." + itemID + ".Item");

        Material material = Material.getMaterial(block);

        ItemStack playerItem = new ItemStack(material);

        ItemMeta playerItemMeta = playerItem.getItemMeta();

        playerItem.setItemMeta(playerItemMeta);
        
        for (int in = 0; in < player.getInventory().getSize(); in ++)
        {
            if (player.getInventory().getItem( in ) == null)
            {
                if(player.getInventory().contains(playerItem))
                {
                	player.getInventory().addItem(playerItem);
                }                	
                else
                {
                	player.getInventory().setItem( in , playerItem);
                }
                return;
            }
        }
        player.updateInventory();
    }
}